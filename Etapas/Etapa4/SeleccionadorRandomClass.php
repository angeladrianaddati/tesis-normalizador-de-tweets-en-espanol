<?php

/**
@file SeleccionadorRandomClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase SeleccionadorRandomClass. Clase desarrollada para selección del candidato a normalizar. Lo hace de forma aleatoria.
*/


namespace Etapas\Etapa4;

/**
 *  SeleccionadorRandomClass. Clase desarrollada para selección del candidato a normalizar. Lo hace de forma aleatoria.
 */
use \Lib\LibInt\MemoriaIntermedioClass;
use \Lib\LibInt\ModuloAbstractClass;

class SeleccionadorRandomClass  extends ModuloAbstractClass
{


 	/**
	* @brief Imlementación de método abstracto. El programá principal llamará a este método. Del listado de candidatos seleccionara al correcto, por cada palabra OOV. Usará una función random para ello.
	* @see ejectur().
	* @return nothing.
	*/
	function ejecutar()
	{
		$this -> entrar_debug("ejecutar");

		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer(); // Se lee la entrada y se la transforma a minuscula para evitar inconvenientes respecto al case sensitive.
	
		$arr_unificacion = array();

		foreach ($entrada as &$mensaje){
			foreach ($mensaje as &$token){
				if($token[1] == false and count($token[2])>2){
					shuffle($token[2]);
				}
			}
		}
		
		print_r($entrada);
		$this -> salida -> escribir($entrada);
		$this -> salir_debug("ejecutar");

	}
}
