<?php

/**
@file SeleccionadorDistanciaEdicion.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase SeleccionadorDistanciaEdicionClass. Clase desarrollada para selección del candidato a normalizar. Lo hace por peso, distancia edición y orden alfábetico.
 */

namespace Etapas\Etapa4;

/**
 *  SeleccionadorDistanciaEdicionClass. Clase desarrollada para selección del candidato a normalizar. Lo hace por peso, distancia edición y orden alfábetico.
 */
use \Lib\LibInt\ArchivoClass;
use \Lib\LibInt\MemoriaIntermedioClass;
use \Lib\LibInt\ModuloEtapa4AbstractClass;

class SeleccionadorDistanciaEdicionClass  extends ModuloEtapa4AbstractClass
{
 	protected	$ruta_pesos =  __DIR__.DIRECTORY_SEPARATOR.'SeleccionadorDistanciaEdicionClass/PesoEtapas.dat'; // Es la ruta en la que se encuentran las prioridades

 	/**
	* @brief Imlementación de método abstracto. El programá principal llamará a este método. Del listado de candidatos seleccionara al correcto, por cada palabra OOV. Usará primero una ponderación o peso por módulo que normalizo. Si hay empate usará la distancia de edición. Y si nuevamente hay empate las ordenara por orden alfábetico.
	* @return nothing.
	*/
	function ejecutar()
	{
		$this -> entrar_debug("ejecutar");

		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer(); // Se lee la entrada y se la transforma a minuscula para evitar inconvenientes respecto al case sensitive.
	
		$archivo_peso = new ArchivoClass();
		$archivo_peso->set_debug($this->debug);
		$archivo_peso->set_ruta($this -> ruta_pesos);
		if ($archivo_peso->existe() == false) {
			$this -> warn_debug("ejecutar: Ruta al archivo de peso por etapas no existe no existe.");
		}
		else
		{
			$str = $archivo_peso -> leer();
			$list_peso =  preg_split("/[\n\r]+/",$str); //Separación por salto de línea y espacio en blanco. 
			foreach ($list_peso as &$modulo){
				$modulo = preg_split("/[\s]+/",$modulo);
			}
			
		foreach ($entrada as &$mensaje){
			foreach ($mensaje as &$token){
				if($token[1] == false and count($token[2])>2){
					$arr_tmp = array();
					foreach ($token[2] as $candidato){
						$orden2 = levenshtein($token[0],$candidato[0]);
						$orden1 =  (int)$list_peso[array_search($candidato[1], array_column($list_peso, 0))][1];

						$arr_tmp[] = array($orden1,$orden2,$candidato[0],$candidato[1]); 

					}

				
				array_multisort(array_column($arr_tmp,0),SORT_ASC,array_column($arr_tmp,1),SORT_ASC,array_column($arr_tmp,2),SORT_ASC,$arr_tmp);

				$token[2]= array();
				foreach ($arr_tmp as $candidato){
					$token[2][] = array($candidato[2],$candidato[3]);
				}

				//print_r($arr_tmp);
				//print_r($token[2]);
				}
			}
		}
		
		$this -> salida -> escribir($entrada);
		$this -> salir_debug("ejecutar");

	}
	}
}
