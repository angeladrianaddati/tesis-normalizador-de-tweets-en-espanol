<?php
/**
* @file CargarTweets.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Lees mensajes de Twitter y los pasa a archivos. Internamente se puede configurar la cantidad, frecuencia y demás datos para realizar la búsqueda y bajada.
*/

print_r("\n");
print_r("**** Simple Twitter API Test - Carga de mensajes de Twitter ****\n\n");

require_once('TwitterAPIExchange.php');
require_once('xmlAPI.php');

$salida = 'Entrada/entrada_validacion.xml';
$cantidad = 1;
$wait = 2;

$count = 10;

print_r("Cantidad de mensajes por cada request: ".$count."\n" );
print_r("Espera entre mensajes: ".$wait."\n" );
print_r("Ruta de salida: ".$salida."\n"  );
print_r("Cantidad de archivos de salida (1 por cada request): ".$cantidad."\n" );


$v_xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" '. 'standalone="yes"?><mensajes/>');
		
for($i=$cantidad; $i>=1; $i--)
{
/** Set access tokens here - see: https://dev.twitter.com/apps/ **/

try {

/** Set access tokens here - see: https://dev.twitter.com/apps/ **/
$settings = array(
    'oauth_access_token' => "",
    'oauth_access_token_secret' => "",
    'consumer_key' => "",
    'consumer_secret' => ""
);

//$url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
//$url = "https://api.twitter.com/1.1/geo/search.json";
$url = "https://api.twitter.com/1.1/search/tweets.json";
$requestMethod = "GET";
 
//$getfield = '?screen_name=iagdotme&count=20';
$getfield = '?q=&geocode=-63.915451632229,-37.0875635,3694km&lang=es&count='.$count;

$twitter = new TwitterAPIExchange($settings);
$string = json_decode($twitter->setGetfield($getfield)
->buildOauth($url, $requestMethod)
->performRequest(),$assoc = TRUE);


if(array_key_exists("errors",$string) && $string["errors"][0]["message"] != "") {
	print_r("Sorry, there was a problem. Twitter returned the following error message: ".$string[errors][0]["message"]);
	exit();
}

//echo "<pre>";
//print_r($string);


//creating object of SimpleXMLElement
$xml_user_info = new SimpleXMLElement("<?xml version=\"1.0\"?><user_info></user_info>");

//function call to convert array to xml
array_to_xml($string,$xml_user_info);

//saving generated xml file
$xml_file = $xml_user_info->asXML('users.xml');

//success and error message based on xml creation
if($xml_file){
    echo print_r("XML file have been generated successfully...\n");
}else{
    echo print_r("XML file generation error...\n");
}

} catch (Exception $e) {
    print_r('Excepción capturada: '.  $e->getMessage(). "\n");
}


//Abrir fichero xml
// El fichero test.xml contiene un documento XML con un elemento raíz y, al
// menos, un elemento /[raiz]/titulo.

if (file_exists('users.xml')) {
    $xml = simplexml_load_file('users.xml');
 
   // print_r($xml);
} else {
	print_r("Error abriendo users.xml. \n");
    exit('Error abriendo users.xml.');
}


/*
echo $xml->statuses->item[0]->text;
 */

/* Para cada <personaje>, se muestra cada <nombre>.
foreach ($xml->statuses->item as $item) {
   echo $item->text.'</br>';
}
 */
	//	$p_ruta_entrada = $salida;
		
	//	if (file_exists($p_ruta_entrada)) {
//			$e_xml = simplexml_load_file($p_ruta_entrada);
	//			}
	//else {
	//	echo "Ruta mala";
	//}



foreach ($xml->statuses->item as $item) {
			if ($item->text<>"")
			$nuevo_nodo = $v_xml -> addChild("mensaje",$item->text);
		}
$count++;
sleep($wait);
}

if ($v_xml -> asXML($salida)==false)
		{
			print_r("El archivo de e/s no se puedo actualizar.");					
		};		
		

?>


