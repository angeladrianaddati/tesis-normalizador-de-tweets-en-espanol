<?php
/**
* @file autoloader.php
* @brief Archivo que contiene la función array_to_xml. Transforma un arreglo en un xml. 
*/

/**
* @brief Transforma un arreglo en un xml.
* @param $array. Arreglo.
* @param &$xml_user_info. Pasado por referencia. A través de la variable se devuelve el xml..
* @return nothing.
*/	
function array_to_xml($array, &$xml_user_info) {
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)){
                $subnode = $xml_user_info->addChild("$key");
                array_to_xml($value, $subnode);
            }else{
                $subnode = $xml_user_info->addChild("item");
                array_to_xml($value, $subnode);
            }
        }else {
            if(!is_numeric($key)){
            	$xml_user_info->addChild("$key",htmlspecialchars("$value"));
            }else{
             	$xml_user_info->addChild("item",htmlspecialchars("$value"));
            }
        }
    }
}

?>
