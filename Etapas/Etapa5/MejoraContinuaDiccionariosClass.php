<?php

/**
@file MejoraContinuadiccionariosClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase MejoraContinuaDiccionariosClass. Clase desarrollada para generar ir actualizando la lista negra de palabras OOV y futuros candidatos, para de forma automatica se solicite por consola validar OOV recurrentes. 
 */

namespace Etapas\Etapa5;

/**
 *  MejoraContinuaDiccionariosClass. Clase desarrollada para generar ir actualizando la lista negra de palabras OOV y futuros candidatos, para de forma automatica se solicite por consola validar OOV recurrentes. 
 */

use \Lib\LibInt\ArchivoClass;
use \Lib\LibInt\ModuloAbstractClass;

class MejoraContinuaDiccionariosClass extends ModuloAbstractClass
{

 	/**
	*  @brief Imlementación de método abstracto. El programá principal llamará a este método. Verificará todas las palabras detectadas como OOV. Si está en la lista negra se la desestima, sino se la agrega a la ListaDeMejoraContinua. Si tiene más de n ocurrencias se solicita por consola validar si es una palabra IV para agregarla al diccionario de la Etapa 2. 
	* @return nothing.
	*/	
	function ejecutar()
	{
		$this -> entrar_debug("ejecutar");

		$ruta_base = "./Etapas/Etapa5/MejoraContinuaDiccionariosClass/"; // Es la ruta en la que se encuentran los diccionarios
		$lista_negra = "ListaNegra.dic";
		$mejora_continua = "ListaMejoraContinua.dic";
		$diccionario = "./Etapas/Etapa2/DetectorPalabras2Class/DiccionarioMejoraContinua.dic";
		$corte = 0;
		$sugerir = array();

		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada = $this -> salida -> leer();
			

		// Se agrega la palabra en minuscula. Ademas se aplica el criterio de sensibilidad a tildes.
		foreach($entrada as &$mensajes) {
			foreach($mensajes as &$token_entrada) {
					$token_entrada[5] = mb_strtolower($token_entrada[0],"utf-8");
			}
		}

			$archivo_lista_negra = new ArchivoClass();
			$archivo_lista_negra->set_debug($this->debug);
			$archivo_lista_negra->set_ruta($ruta_base.$lista_negra);
			
			$archivo_mejora = new ArchivoClass();
			$archivo_mejora->set_debug($this->debug);
			$archivo_mejora->set_ruta($ruta_base.$mejora_continua );
			
			$archivo_diccionario = new ArchivoClass();
			$archivo_diccionario->set_debug($this->debug);
			$archivo_diccionario->set_ruta($diccionario );
			
			if ($archivo_lista_negra->existe() == false or $archivo_mejora->existe() ==false or $archivo_mejora->existe() ==false){
				$this -> warn_debug("ejecutar: Ruta al diccionario no existe.");
				echo "perdedor";
			}
			else
			{				
				$list_mejora =  preg_split("/[\s]+/",mb_strtolower($archivo_mejora -> leer(),"utf-8")); //Separación por salto de línea y espacio en blanco. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.
				$list_negra = preg_split("/[\s]+/",mb_strtolower($archivo_lista_negra -> leer(),"utf-8")); //Separación por salto de línea y espacio en blanco. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.
				
				foreach($entrada as &$mensajes) {
				foreach ($mensajes as &$token_entrada) {
					if($token_entrada[1]==false){
						$pos = array_search($token_entrada[5],$list_negra ,TRUE);
						//$pos = $this->busqueda_dicotomica($list_palabras_diccionario,0,$cantidad,$token_entrada[5]);
						//asort($palabras);	
						if ($pos == false) {
							
							//$pos = array_search($token_entrada[5],$list_mejora,TRUE);
							if ($this->contar_valores($list_mejora,$token_entrada[5])+1>=$corte){
								$sugerir[]=$token_entrada[5];
							};
							$archivo_mejora-> escribir("\r\n".$token_entrada[5]);
							//break;
						}
					}
					}
				}	
			}
			//print_r($sugerir);
			$sugerir = array_unique($sugerir);
			//print_r($sugerir);
			print_r("\n");
			print_r("\n");
			foreach($sugerir as $palabra){
				//echo "hola";
				$entrada = "";
				while ($entrada <> 'Y' and $entrada <> 'N' and $entrada <> 'y' and $entrada <> 'n'){
					print_r("Es '".$palabra."' una palabra dentro del lenguaje(Y/N)?  ");
					$entrada = readline("");
				}
				if ($entrada == "Y" or $entrada == "y"){
					echo "entro";
					$archivo_diccionario -> escribir("\r\n".$palabra);
					
				}
				else{
					$archivo_lista_negra-> escribir("\r\n".$palabra);
				}
				//$list_mejora = borrar_valores($list_mejora,$palabra);
			}
		}
				
 	/**
	* @brief Recibe un arreglo de palabras y una palabra a buscar y devuelve la cantidad de veces que se encuentra repetida la palarba.
	* @param $a. Arreglo de Palabras.
	* @param $buscado. String con la palabra a buscar.
	* @return integer.
	*/		
	protected function contar_valores($a,$buscado) {
	  if(!is_array($a)) return NULL;
	  $i=0;
	  foreach($a as $v)
	   if($buscado===$v)
		$i++;
	  return $i;
	 }

 	/**
	* @brief Borra todas las ocurrencias de una palabra en un arreglo.
	* @param $a. Arreglo de Palabras.
	* @param $buscado. String con la palabra a buscar.
	* @return arreglo.
	*/	 
	 protected function borrar_valores($a,$buscado) {
	  if(!is_array($a)) return NULL;
	  $i=0;
	  $count = count($a);
	  while($i<$count){
	   if($buscado===$a[$i])
		unset($a[$i]);
		$i++;
	}
	  return $array_values($a);
	 }

}



/*

public static function removeEmoji($text) {

    $clean_text = "";

    // Match Emoticons
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clean_text = preg_replace($regexEmoticons, '', $text);

    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clean_text = preg_replace($regexSymbols, '', $clean_text);

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clean_text = preg_replace($regexTransport, '', $clean_text);

    // Match Miscellaneous Symbols
    $regexMisc = '/[\x{2600}-\x{26FF}]/u';
    $clean_text = preg_replace($regexMisc, '', $clean_text);

    // Match Dingbats
    $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
    $clean_text = preg_replace($regexDingbats, '', $clean_text);

    return $clean_text;
}
*/
