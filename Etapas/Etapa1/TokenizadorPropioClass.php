<?php

/**
* @file TokenizadorPropioClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase TokenizadorPropioClass. Clase desarrollada para el tokenizado de un texto. Lee la entrada de un archivo intermedio y la escribé en el. BASADO EN EL TOKENIZADOR DE NLP_TOOLS
 */
namespace Etapas\Etapa1;

/**
 *  TokenizadorPropioClass. Clase desarrollada para el tokenizado de un texto. Lee la entrada de un archivo intermedio y la escribé en el. BASADO EN EL TOKENIZADOR DE NLP_TOOLS
 */

use \Lib\LibInt\ModuloAbstractClass;

class TokenizadorPropioClass extends ModuloAbstractClass
{

 	/**
	* @brief Implementación de método abstracto. El programá principal llamará a este método. Realizará el tokenizado del texto de entrada.
	* @return nothing.
	*/
	function ejecutar()
	{
		$this -> entrar_debug("ejecutar");
		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$mensajes = $this -> salida -> leer();

		// for the character classes
		// see http://php.net/manual/en/regexp.reference.unicode.php
		//$pat = '/[\pZ\pC]+/u';
		$pat = '/[\pZ]+/u';
		$pat2 = '/
		            (
(\-|\+|\#|\@|\$){0,1}(\p{L}|\p{N}|[-_])+(\p{L}|\p{N}|[\w#!:.?+=&%@!\/-_])*(\p{L}|\p{N})+(\%){0,1}
| (\-|\+|\#|\@){0,1}\\p{L}|\p{N}(\%){0,1}    
| \p{P} 
|([\x{1F600}-\x{1F64F}])
|([\x{1F300}-\x{1F5FF}])
|([\x{1F680}-\x{1F6FF}])
|([\x{2600}-\x{26FF}])
|([\x{2700}-\x{27BF}])	
|([\x{1F1E6}-\x{1F1FF}])
|([\x{1F910}-\x{1F95E}])
|([\x{1F980}-\x{1F991}])
|([\x{1F9C0}])
|([\x{1F9F9}]))
|\S
		        /xu';

		$arr0 = array();

		foreach ($mensajes as $mensaje) {
			$arr1 = array();
			foreach ($mensaje as $token) {
				$arr = preg_split($pat,$token[0],null,PREG_SPLIT_NO_EMPTY);
//				print_r($arr );
				$arr_tmp = array();
				$arr2 = array();
				foreach ($arr as $palabras){
					 preg_match_all($pat2,$palabras,$arr_tmp);
					
					 $arr2 = array_merge($arr2,$arr_tmp[0]);
				}
				 //print_r($arr2); 
				//print_r($arr); exit();
				//exit();

				$arr3 = array();
				foreach($arr2 as $string) {
					if ($string != null and trim($string)!='\n' and strlen($string)>0 and strlen($string)>0)
						array_push($arr3,array($string));	
				}
//				for ($i=0;$i<count($arr3); $i++){
//					if ( $arr3[$i]== '') unset($arr3[$i]);
//				}
				$arr3 = array_values($arr3);
				array_push($arr0,$arr3);
			}
			//array_push($arr0,$arr1);
		}
		 
		$this -> salida -> escribir($arr0);
		//print_r($arr[2]);
		//$this->archivo_intermedio->escribir($arr[2]);
		$this -> salir_debug("ejecutar");
	}

}



/*

public static function removeEmoji($text) {

    $clean_text = "";

    // Match Emoticons
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clean_text = preg_replace($regexEmoticons, '', $text);

    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clean_text = preg_replace($regexSymbols, '', $clean_text);

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clean_text = preg_replace($regexTransport, '', $clean_text);

    // Match Miscellaneous Symbols
    $regexMisc = '/[\x{2600}-\x{26FF}]/u';
    $clean_text = preg_replace($regexMisc, '', $clean_text);

    // Match Dingbats
    $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
    $clean_text = preg_replace($regexDingbats, '', $clean_text);

    return $clean_text;
}
*/
