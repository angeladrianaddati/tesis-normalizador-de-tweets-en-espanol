<?php

/**
* @file DetectorPalabrasInglesClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase DetectorPalabrasInglesClass. Clase desarrollada para la detección de palabras de uso común en ingles.
 */
namespace Etapas\Etapa2;


use \Lib\LibInt\ArchivoClass;
use \Lib\LibInt\ModuloEtapa2AbstractClass;

/**
 *  DetectorPalabrasInglesClass. Clase desarrollada para la detección de palabras de uso común en ingles.
 */
class DetectorPalabrasInglesClass extends ModuloEtapa2AbstractClass{
	protected	$ruta_base = __DIR__.DIRECTORY_SEPARATOR.'/DetectorPalabrasInglesClass/'; // Es la ruta en la que se encuentran los diccionarios
	protected	$list_diccionarios = array("DiccionarioIngles.dic");
	protected	$diccionarios = array();

 	/**
	* @brief Implementación de método abstracto. El programá principal llamará a este método. Realizará la detección de las palabras en ingles buscando en diccionarios preestablecidos.
	* @return nothing.
	*/
	function ejecutar()
	{
		$this -> entrar_debug("ejecutar");
	

		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada = $this -> salida -> leer();
		
		if (count($this->diccionarios)==0){
			$this->inicializar_diccionarios();
		}


		// Se agrega la palabra en minuscula. Ademas se aplica el criterio de sensibilidad a tildes.
		foreach($entrada as &$mensajes) {
			foreach($mensajes as &$token_entrada) {
					if ($this->acent_sensitive == true) {
						$token_entrada[5] = mb_strtolower($token_entrada[0],"utf-8");
					}
					else{
						$token_entrada[5] = str_replace($arr_acent, $arr_acent_replace,mb_strtolower($token_entrada[0],"utf-8"));
					}
					
					if (!array_key_exists(6,$token_entrada)){
						$token_entrada[6] = array();
					}
			}
		}
		
		$i = 0;
		while ($i<count($this->diccionarios)){
			$list_palabras_diccionario = $this->diccionarios[$i];
						
			//echo "noooo entroooooo";
			foreach($entrada as &$mensajes) {
				foreach ($mensajes as &$token_entrada) {
					$pos = array_search($token_entrada[5],$list_palabras_diccionario,TRUE);
					//$pos = $this->busqueda_dicotomica($list_palabras_diccionario,0,$cantidad,$token_entrada[5]);
					//asort($palabras);	
					if ($pos !== false) {
						$token_entrada[1] = true;
						array_push($token_entrada[6],$list_palabras_diccionario[$pos]);
						//break;
					}
				}
			}
					$i++;
		}
		$this -> salida -> escribir($entrada);	
		$this -> salir_debug("ejecutar");
		
	}

public function inicializar_diccionarios(){
		
		// Se recorre cada diccionario. Por cada palabra, se verifica si está dentro del diccionario. Si está dentro del diccionario se la actualiza y se clasifica OOV.
		foreach ($this->list_diccionarios as $dic) {
			$archivo_diccionario = new ArchivoClass();
			$archivo_diccionario->set_debug($this->debug);
			$archivo_diccionario->set_ruta($this -> ruta_base.$dic);
			if ($archivo_diccionario->existe() == false) {
				$this -> warn_debug("ejecutar: Ruta al diccionario no existe.");
			}
			else
			{
				//echo "inicio";
		//		if ($this->acent_sensitive == true) {
					$str = mb_strtolower($archivo_diccionario -> leer(),"utf-8");
					$list_palabras_diccionario =  preg_split("/[\s]+/",$str); //Separación por salto de línea y espacio en blanco. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.
							
				$list_palabras_diccionario = array_values(array_diff($list_palabras_diccionario , array('')));
				
				$this->diccionarios[] = $list_palabras_diccionario;
			}
		}				
	
}
   

}


