<?php

/**
* @file DetectorPalabrasEspañol.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase DetectorPalabrasEspañol. Clase desarrollada para la detección de de palabras en español. Utiliza los métodos auxiliares. 
 */
namespace Etapas\Etapa2;

/**
 *  DetectorPalabrasEspañol. Clase desarrollada para la detección de de palabras en español. Utiliza los métodos auxiliares. 
 */

use \Lib\LibInt\ModuloEtapa2AbstractClass;

class DetectorPalabrasEspañolClass extends ModuloEtapa2AbstractClass
{


 	/**
	* @brief Implementación de método abstracto. El programá principal llamará a este método. Realizará la detección de las palabras en español ejecutando los módulos auxiliares.
	* @return nothing.
	*/
	function ejecutar()
	{	
		$this -> entrar_debug("ejecutar");
		$this -> auxiliar -> ejecutar();
		$this -> salir_debug("ejecutar");
	}

}
