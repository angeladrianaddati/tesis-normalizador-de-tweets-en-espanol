<?php

/**
* @file NormalizadorLogogramasClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase NormalizadorLogogramasClass. Clase desarrollada para la normalización de palabras que contienen logogramas. 
 */
namespace Etapas\Etapa3;

use \Lib\LibInt\ArchivoClass;
use \Lib\LibInt\MemoriaIntermedioClass;
use \Lib\LibInt\ModuloEtapa3AbstractClass;

/**
 *  NormalizadorLogogramasClass. Clase desarrollada para la normalización de palabras que contienen logogramas. 
 */
class NormalizadorLogogramasClass extends ModuloEtapa3AbstractClass
{

 	/**
	* @brief Imlementación de método abstracto. El programá principal llamará a este método. Intentará normalizar palabras que contienen logogramas.  
	* @return nothing.
	*/
	function ejecutar()
	{

	
		$this -> entrar_debug("ejecutar");

		$ruta_base = "./Etapas/Etapa3/NormalizadorLogogramasClass/"; // Es la ruta en la que se encuentran los diccionarios.

		$arr_acent = array('á','é','í','ó','ú','ä','ë','ï','ö','ü');
		$arr_acent_replace = array('a','e','i','o','u','a','e','i','o','u');
		$longitud_maxima = 6;

		if ($this->salida->existe() == false){
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer();

		$list_reglas = array("ReglasLogogramas.reg");

		$modulo_name =  substr (get_class($this),strrpos(get_class($this),'\\')+1,strlen(get_class($this)));
		
		// Se recorre cada diccionario. Por cada palabra, se verifica si está dentro del diccionario. Si está dentro del diccionario se la actualiza y se clasifica OOV.
		foreach ($list_reglas as $regla) {
			$archivo_regla = new ArchivoClass();
			$archivo_regla->set_debug($this->debug);
			$archivo_regla->set_ruta($ruta_base.$regla);
			if ($archivo_regla->existe() == false) {
				$this -> warn_debug("ejecutar: Ruta al diccionario no existe.");
				//echo "No existe reglas";
			}
			else
			{
				
				
				$list_reglas =  preg_split("/[\n\r]+/",mb_strtolower($archivo_regla -> leer(),"utf-8")); //Separación por salto de línea y espacio en blanco. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.
				foreach($list_reglas as &$regla){
					$arr = array();
					$arr = preg_split("/\|\-\>\|/",mb_strtolower($regla,"utf-8"));
					if ($this->acent_sensitive == true) {
						$arr[0] = str_replace($arr_acent, $arr_acent_replace,$arr[0]);
					}
					$regla = $arr;
				}
				
		$arr0 =  array();	
		//print_r($entrada );
		$i = 0;
		//print_r([$entrada[0][25][0]]);
		//print_r([$entrada[0][26][0]]);
		while ($i<count($entrada)){
			//foreach($entrada as $mensajes) {
			$mensajes = $entrada[$i];
			$j = 0;
			while ($j<count($mensajes)){
				$arr = array();
				//foreach($mensajes2 as $token_entrada) {
				$token_entrada = &$mensajes[$j];
				if ($token_entrada[1] == false and strlen($token_entrada[0])>2)
				{
					$arr = array();

					foreach($list_reglas as &$regla){
						if ((trim($regla[0])!="" and preg_match($regla[0],$token_entrada[0])==true)){
							$matches = array();
							//print_r($regla[0]);
							preg_match_all($regla[0], $token_entrada[0],$matches, PREG_OFFSET_CAPTURE) ;
							$candidatas = array(str_split($token_entrada[0]));
							$matches[0] = array_slice($matches[0],0,$longitud_maxima);
							foreach($matches[0] as $match){
								$i2 = 0;
								$cant = count($candidatas);
								while ($i2<$cant){
									$candidata = $candidatas[$i2];
									//$candidatas[$i] = $candidatas[$i];
									$candidata[$match[1]] = $regla[1];
									for($j2=1;$j2<strlen($match[0]);$j2++){
										$candidata[$match[1]+$j2]="";
									};
									$candidatas[]=$candidata;
									$i2++;
									}
							}
							unset ($candidatas[0]);

							//print_r($candidatas);
							foreach ($candidatas as &$candidata){
								$token_entrada2 = array();
								$token_entrada2[6]=array();
								$token_entrada2['logogramas1']= $i;
								$token_entrada2['logogramas2']= $j;
								$token_entrada2[0] = implode("",$candidata);
								$token_entrada2[1] = false;
								array_push($arr,array($token_entrada2));	

							}
							
							//$candidatas = array_values($candidatas);
							
						}
					}

					if (count($arr)>0) {
						$arr0 = array_merge($arr0,$arr);
					}

				}
				$j++;
			}
			$i++;
		}
		
	}
}

		if (count($arr0)>0)
			{

			$obj = new MemoriaIntermedioClass();
			$obj -> inicializar ($arr0,null,$this->debug);

			$this -> auxiliar -> set_acent_sensitive(false);
			$this -> auxiliar -> set_lazzy(false);

			$this -> auxiliar -> set_salida($obj);
			//$this -> auxiliar -> set_lazzy(true);
			$this -> auxiliar -> ejecutar();

			$result = $obj->leer();

			$i = 0;
			while ($i< count($result)){
				foreach($result[$i] as $candidata){		
					if($candidata[1] ==true) {
						//$key = array_search($candidata, $result[$i]);	
						//$key = array_search(mb_strtolower($arr0[$i][$key][0],"utf-8"), array_column($entrada[$i], 5));
						$arr_tmp = array();
						$i2 =$candidata['logogramas1'];
						$j2 =$candidata['logogramas2'];
						foreach ($candidata[6] as $palabra_candidata) {
							array_push($arr_tmp,array($palabra_candidata,$modulo_name));
						}
						$entrada[$i2][$j2][2] = array_merge($entrada[$i2][$j2][2], $arr_tmp );
						//print_r($entrada);
					}
				}
				$i++;
			}
		}
		
		$this -> salida -> escribir($entrada);
		
		$this -> salir_debug("ejecutar");
	}

}
