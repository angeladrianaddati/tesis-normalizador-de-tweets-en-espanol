<?php

/**
* @file OmisionCaracterClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase OmisionCaracterClass. Clase desarrollada para la normalización de palabras con ausencia de un caracter. Por ejemplo, la palabra hol, la normalizará a hola. 
 */
namespace Etapas\Etapa3;

/**
 *  OmisionCaracterClass. Clase desarrollada para la normalización de palabras con ausencia de un caracter. Por ejemplo, la palabra hol, la normalizará a hola. 
 */
use \Lib\LibInt\MemoriaIntermedioClass;
use \Lib\LibInt\ModuloEtapa3AbstractClass;

class OmisionCaracterClass extends ModuloEtapa3AbstractClass
{

 	/**
	* @brief Imlementación de método abstracto. El programá principal llamará a este método. Intentará normalizar palabras con ausencia de un caracter.
	* @return nothing.
	*/
	function ejecutar()
	{
		$this -> entrar_debug("ejecutar");

		if ($this->salida->existe() == false){
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer();

		$caracteres = range('a', 'z');
		
		$longitud_minima = 2; //longitud minima del token de entrada sobre el que se aplicara el criterio. 


		$modulo_name =  substr (get_class($this),strrpos(get_class($this),'\\')+1,strlen(get_class($this)));
		
		
		$arr0 =  array();	
		//print_r($entrada );
		$i = 0;
		//print_r([$entrada[0][25][0]]);
		//print_r([$entrada[0][26][0]]);
		while ($i<count($entrada)){
			//foreach($entrada as $mensajes) {
			$mensajes = $entrada[$i];
			$j = 0;
			while ($j<count($mensajes)){
				$arr = array();
				//foreach($mensajes2 as $token_entrada) {
				$token_entrada = &$mensajes[$j];
				if ($token_entrada[1] == false and strlen($token_entrada[0])>=$longitud_minima)
				{
					$arr = array();
					foreach($caracteres as $caracter){
						$y = 0;
						while ($y<=strlen($token_entrada[0])){
							$token_entrada2 = array();
							$token_entrada2[6]=array();
							$token_entrada2['omision1']= $i;
							$token_entrada2['omision2']= $j;
							$token_entrada2[0] = substr_replace($token_entrada[0], $caracter, $y, 0);
							$token_entrada2[1] = false;
							array_push($arr,array($token_entrada2));	
							$y ++;							
						}
					}
					
					//print_r($arr);

					if (count($arr)>0) {
						$arr0 = array_merge($arr0,$arr);
					}
				}
				$j++;
			}
			$i++;
		}
		
		if (count($arr0)>0)
		{
			$obj = new MemoriaIntermedioClass();
			$obj -> inicializar ($arr0,null,$this->debug);

			

			$this -> auxiliar -> set_salida($obj);
			$this -> auxiliar -> set_acent_sensitive(false);
			$this -> auxiliar -> set_lazzy(false);
			$this -> auxiliar -> ejecutar();

			$result = $obj->leer();

			$i = 0;
			while ($i< count($result)){
				foreach($result[$i] as $candidata){		
					if($candidata[1] ==true) {
						//$key = array_search($candidata, $result[$i]);	
						//$key = array_search(mb_strtolower($arr0[$i][$key][0],"utf-8"), array_column($entrada[$i], 5));
						$arr_tmp = array();
						$i2 =$candidata['omision1'];
						$j2 =$candidata['omision2'];
						foreach ($candidata[6] as $palabra_candidata) {
							array_push($arr_tmp,array($palabra_candidata,$modulo_name));
						}
						$entrada[$i2][$j2][2] = array_merge($entrada[$i2][$j2][2], $arr_tmp );
						//print_r($entrada);
					}
				}
				$i++;
			}
		}

		$this -> salida -> escribir($entrada);
		
		$this -> salir_debug("ejecutar");
	}

}
