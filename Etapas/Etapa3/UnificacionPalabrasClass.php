<?php

/**
* @file UnificacionPalabrasClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase UnificacionPalabrasClass. Clase desarrollada para la normalización de palabras escritas con espacios en el medio. Tendrá que unir varios tokens. Ejemplo: debería resolver problemas tales como: ho la devolviendo hola. 
 */

namespace Etapas\Etapa3;


use \Lib\LibInt\MemoriaIntermedioClass;
use \Lib\LibInt\ModuloEtapa3AbstractClass;

/**
 *  UnificacionPalabrasClass. Clase desarrollada para la normalización de palabras escritas con espacios en el medio. Tendrá que unir varios tokens. Ejemplo: debería resolver problemas tales como: ho la devolviendo hola. 
 */
class UnificacionPalabrasClass  extends ModuloEtapa3AbstractClass
{
	
 	/**
	* @brief Imlementación de método abstracto. El programá principal llamará a este método. Intentará normalizar palabras escritas con espacios en el medio. Tendrá que unir varios tokens.
	* @return nothing.
	*/
	function ejecutar()
	{
		$modulo_name =  substr (get_class($this),strrpos(get_class($this),'\\')+1,strlen(get_class($this)));

		$this -> entrar_debug("ejecutar");

		$ruta_base = "./Etapas/Etapa3/UnificacionPalabrasClass/"; // Es la ruta en la que se encuentran archivos auxiliares.
		$longitud_maxima = 2; // Es la longitud máxima de palabras que se podrán unir. 
		
		$arr_acent = array('á','é','í','ó','ú','ä','ë','ï','ö','ü');
		$arr_acent_replace = array('a','e','i','o','u','a','e','i','o','u');
		

		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer(); // Se lee la entrada y se la transforma a minuscula para evitar inconvenientes respecto al case sensitive.

		$list_reglas = array("ReglasOnomatopeyas.reg");

//print_r($entrada);

		// Se agrega la palabra en minuscula. Ademas se aplica el criterio de sensibilidad a tildes.
		/*foreach($entrada as &$mensajes) {
			foreach($mensajes as &$token_entrada) {
					if ($this->acent_sensitive == true) {
						$token_entrada[5] = mb_strtolower($token_entrada[0],"utf-8");
					}
					else{
						$token_entrada[5] = str_replace($arr_acent, $arr_acent_replace,mb_strtolower($token_entrada[0],"utf-8"));
					}
					
					if (!array_key_exists(6,$token_entrada)){
						$token_entrada[6] = array();
					}
			}
		}*/
	
		$arr_unificacion = array();
		
		$h = 0;
		$ind_tmp = 0;
		$arr_tmp = array();
		while ($h< count($entrada)){
			$mensajes = $entrada[$h];			
			$i = 0;
			while ($i<count($mensajes)) {

				$j = 0;
		
				$band = true;
				$es_iv = true;
				while ($i+$j< count($mensajes) and $band==true) {
					$band = $mensajes[$i+$j][1];
					$j++;
				}
	
				$j--;
				
				if ($i+$j-$longitud_maxima>=0){
					$k = $j-$longitud_maxima;
				} else{
					$k = 0;
				}
				
				//$arr_unificacion = array();
				while ($k<=$j) {
					$tmp = "";
					$tmp2 = "";
					$k2 = $k;
					$band2 = true;
					while ($k2<$longitud_maxima and $i+$k2< count($mensajes)){
						$tmp .= $mensajes[$i+$k2][0];
						$tmp2 .= " ".$mensajes[$i+$k2][0];
						$k2++;
					}
					
					//print_r("\n".$tmp.$k2);
					
					$k2--;
					//echo "\n";
					while ($k2>$k+1 ){
							//echo "-".$tmp."-";
							$arr = array($tmp,false,array(),"","");
							//array_push($arr);
							//$arr[0] = $tmp;
							$arr[1] = false;
							$arr['unificacion7'] = $tmp;
							$arr['unificacion8'] = $i+$k;
							$arr['unificacion9'] = $i+$k2;
							$arr['unificacion10'] = $h;
							$arr['unificacion11'] = $tmp2;
							array_push($arr_unificacion,$arr);
							$tmp = substr($tmp,0,strlen($tmp)-strlen($mensajes[$i+$k2][0]));
							$tmp2 = trim(substr($tmp,0,strlen($tmp2)-strlen($mensajes[$i+$k2][0])));
							$k2--;
					}
					
					$k++;
					
				}			
		
				$i = $i + $j +2;
			}
			$h++;
		}		
		
		if (count($arr_unificacion)>0) {
			$obj = new MemoriaIntermedioClass();
			$obj -> inicializar (array($arr_unificacion),null,$this->debug);
			$this -> auxiliar -> set_salida($obj);
			$this -> auxiliar -> set_acent_sensitive(false);
			$this -> auxiliar -> set_lazzy(false);
			$this -> auxiliar -> set_modulos (array('DetectorPalabrasClass',  'DetectorEntidadesNombradasClass', 'DetectorEmoticonesClass',  'DetectorOnomatopeyasClass', 'DetectorFechaHoraClass', 'DetectorNumerosClass'));
			//array('DetectorPalabrasClass', 'DetectorEntidadesNombradasClass', 'DetectorEmoticonesClass', 'DetectorUrlEmailClass', 'DetectorOnomatopeyasClass', 'DetectorFechaHoraClass', 'DetectorNumerosClass', 'DetectorMetalenguajePlataformaSocialClass')
		//print_r("UNIFICACION");
			$this -> auxiliar -> ejecutar();

			$result = $obj->leer();

		
			$i = 0;
			$count0 = count($result);

			while ($i< $count0){
				$j = 0;
				$count1 = count($result[$i]);
				while ($j<$count1){
					//print_r("ij".$i.$j);
					$band = false;
					while ($j<$count1 and $band == false) {
						//print_r($result[$i][$j]);
						//print_r("ijj".$i.$j);
						$band = $result[$i][$j][1];
						$j++;
					}
					$j--;
					if ($band == true){
						  
							$candidata = $result[$i][$j];

							$mensajes = &$entrada[$candidata['unificacion10']];
							$token =  &$mensajes[$candidata['unificacion8']];
							$token[1] = false;
							$token[2] = array(array($candidata[0],$modulo_name));						
							$count = count($mensajes);
							$n = 	$candidata['unificacion8']+1;
							//print_r($mensajes[$candidata[8]]);		
							while($n<$count and $n<= $candidata['unificacion9']) {
								//print_r($mensajes[$n][0]);
								$token[0] .= " ".$mensajes[$n][0]; 
								unset($mensajes[$n]);
								$n++;
							}

					}
					$j++;
					while($j<$count1 and $result[$i][$j]['unificacion10']==$candidata['unificacion10'] and $result[$i][$j]['unificacion8']<=$candidata['unificacion9']){
						$j++;
					}
					//print_r("j".$j);
					
				}
				$i++;
			}
			
			foreach($entrada as &$mensajes) {
				$mensajes = array_values($mensajes);
			}


			$this -> salida -> escribir($entrada);
		}
		
		$this -> salir_debug("ejecutar");

	}
}
