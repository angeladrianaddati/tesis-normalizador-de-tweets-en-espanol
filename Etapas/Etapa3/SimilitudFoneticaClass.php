<?php

/**
* @file SimilitudFoneticaClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase YuxtaposiciónPalabrasClass. Clase desarrollada para la normalización de palabras escritas sin separacion de espacio. Tendrá que separar un tokens en varios. Ejemplo: debería resolver problemas caminoacasa devolviendo camino a casa. 
 */

namespace Etapas\Etapa3;


use \Lib\LibInt\MemoriaIntermedioClass;
use \Lib\LibInt\ModuloEtapa3AbstractClass;

/**
 *  SimilitudFoneticaClass. Clase desarrollada para la normalización de palabras cuyo sonido es similar a una palabra IV.
 */
class SimilitudFoneticaClass extends ModuloEtapa3AbstractClass
{
	
	 /**
	* @brief Imlementación de método abstracto. El programá principal llamará a este método. Tratará las palabras cuyo sonido es similar a una palabra IV.
	* @return nothing.
	*/

	function ejecutar()
	{
		
		$modulo_name =  substr (get_class($this),strrpos(get_class($this),'\\')+1,strlen(get_class($this)));


		$this -> entrar_debug("ejecutar");

		if ($this->salida->existe() == false){
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer(); // Se lee la entrada y se la transforma a minuscula para evitar inconvenientes respecto al case sensitive.
	
		// Se agrega la palabra en minuscula. Ademas se aplica el criterio de sensibilidad a tildes.
		/*foreach($entrada as &$mensajes) {
			foreach($mensajes as &$token_entrada) {
					$token_entrada[5] = mb_strtolower($token_entrada[0],"utf-8");				
			}
		}*/
		
		$h = 0;
		while ($h< count($entrada)){	
			$i = 0;
			while ($i<count($entrada[$h])) {
				$entrada[$h][$i][5] = mb_strtolower($entrada[$h][$i][0],"utf-8");
				$i++;
			}
			$h++;
		}

		$arr0 =  array();	
		//print_r($entrada );
		$i = 0;
		//print_r([$entrada[0][25][0]]);
		//print_r([$entrada[0][26][0]]);
		while ($i<count($entrada)){
		//foreach($entrada as $mensajes) {
			$mensajes = $entrada[$i];
			$j = 0;
			while ($j<count($mensajes)){
			$arr = array();
			//foreach($mensajes2 as $token_entrada) {
				$token_entrada = $mensajes[$j];
				//print_r( $token_entrada[0].$i.$j);
					if ($token_entrada[1] == false and strlen($token_entrada[0])>1) {
						$token_entrada2 = $token_entrada;
						$token_entrada2[6]=array();
						$token_entrada2['fonetica1']= $i;
						$token_entrada2['fonetica2']= $j;
						array_push($arr,$token_entrada2);
						array_push($arr0,$arr);
					}			
					$j++;
					
			}
			$i++;

		}
	
		if (count($arr0)>0) {	
					//print_r($arr0);
					
			$obj = new MemoriaIntermedioClass();
			$obj -> inicializar ($arr0,null,$this->debug);

			

			$this -> auxiliar -> set_salida($obj);
			$this -> auxiliar -> set_acent_sensitive(false);
			$this -> auxiliar -> set_levenshtein(false);
			$this -> auxiliar -> set_fonetic(true);
			$this -> auxiliar -> set_lazzy(false);
			$this -> auxiliar -> ejecutar();

			$result = $obj->leer();

			$i = 0;
			while ($i< count($result)){
				foreach($result[$i] as $candidata){		
					if($candidata[1] ==true) {
						//$key = array_search($candidata, $result[$i]);	
						//$key = array_search(mb_strtolower($arr0[$i][$key][0],"utf-8"), array_column($entrada[$i], 5));
						$arr_tmp = array();
						$i2 =$candidata['fonetica1'];
						$j2 =$candidata['fonetica2'];
						foreach ($candidata[6] as $palabra_candidata) {
							array_push($arr_tmp,array($palabra_candidata,$modulo_name));
						}
						$entrada[$i2][$j2][2] = array_merge($entrada[$i2][$j2][2], $arr_tmp );
						//print_r($entrada);
					}
				}
				$i++;
			}
			$this -> auxiliar -> set_fonetic(false);
			
			//print_r($entrada_minuscula);
			$this -> salida -> escribir($entrada);
			
			$this -> salir_debug("ejecutar");
		}
	}

}

