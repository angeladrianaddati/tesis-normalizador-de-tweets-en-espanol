<?php

/**
* @file RepeticionCaracteresClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase RepeticionCaracteresClass. Clase desarrollada para la normalización de palabras con caracteres repetidos. Por ejemplo, la palabra hoooooooola, la normalizará a hola. 
 */

namespace Etapas\Etapa3;

/**
 *  RepeticionCaracteresClass. Clase desarrollada para la normalización de palabras con caracteres repetidos. Por ejemplo, la palabra hoooooooola, la normalizará a hola. 
 */
use \Lib\LibInt\MemoriaIntermedioClass;
use \Lib\LibInt\ModuloEtapa3AbstractClass;

class RepeticionCaracteresClass extends ModuloEtapa3AbstractClass
{

 	/**
	* @brief Imlementación de método abstracto. El programá principal llamará a este método. Intentará normalizar palabras con caracteres repetidos
	* @return nothing.
	*/
	function ejecutar()
	{

		$modulo_name =  substr (get_class($this),strrpos(get_class($this),'\\')+1,strlen(get_class($this)));
		
		$this -> entrar_debug("ejecutar");

		if ($this->salida->existe() == false){
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer();

		$caracteres = range('a', 'z');


		// Se agrega la palabra en minuscula. Ademas se aplica el criterio de sensibilidad a tildes.
		/*foreach($entrada as &$mensajes) {
			foreach($mensajes as &$token_entrada) {
					$token_entrada[5] = mb_strtolower($token_entrada[0],"utf-8");				
			}
		}*/
		
		$h = 0;
		while ($h< count($entrada)){	
			$i = 0;
			while ($i<count($entrada[$h])) {
				$entrada[$h][$i][5] = mb_strtolower($entrada[$h][$i][0],"utf-8");
				$i++;
			}
			$h++;
		}
		

		$obj = new MemoriaIntermedioClass();
		$obj -> inicializar (array(),array(),$this->debug);
		$this -> auxiliar -> set_salida($obj);
		$this -> auxiliar -> set_acent_sensitive(false);
		$this -> auxiliar -> set_lazzy(false);
		
		foreach($entrada as &$mensajes) {
			foreach($mensajes as &$token_entrada) {

				if ($token_entrada[1] == false)
				{
					$arr = array();


					foreach($caracteres as $caracter){
						if ( preg_match("/".$caracter."{2,}/",  $token_entrada[5])){
							array_push($arr,array(array(preg_replace("/".$caracter."{2,}/",$caracter, $token_entrada[5]),false,array())));	
							foreach($arr as $candidato){
								if ( preg_match("/".$caracter."{2,}/",$candidato[0][0])){
									array_push($arr,array(array(preg_replace("/".$caracter."{2,}/",$caracter, $candidato[0][0]),false,array())));	
								}
							}
						}
					
					// Vefico si hay candidatos de repeticion reemplazando por 2 caracteres iguales seguidos
						if ( preg_match("/".$caracter."{3,}/",  $token_entrada[5])){
							array_push($arr,array(array(preg_replace("/".$caracter."{3,}/",$caracter.$caracter, $token_entrada[5]),false,array())));
							foreach($arr as $candidato){
								if ( preg_match("/".$caracter."{3,}/",$candidato[0][0])){
									array_push($arr,array(array(preg_replace("/".$caracter."{3,}/",$caracter.$caracter, $candidato[0][0]),false,array())));
								}
							}
		
						}
					}
										
					if (count($arr)>0) {
						$obj->escribir($arr);
						$this -> auxiliar -> set_salida($obj);
						$this -> auxiliar -> ejecutar();
						//print_r($obj->leer());
						foreach($obj->leer() as $candidata){
							if($candidata[0][1] ==true) {
								array_push($token_entrada[2],array($candidata[0][0],$modulo_name ));
							}
						}
					}

				}
			}
		}
		

		$this -> salida -> escribir($entrada);
		
		$this -> salir_debug("ejecutar");
	}

}
