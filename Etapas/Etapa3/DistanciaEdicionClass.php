<?php

/**
* @file DistanciaEdicionClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase DistanciaEdicionClass. Clase desarrollada para la normalización de palabras usando el criterio de distancia de edición. Por ejemplo, la palabra pruebax la normalizara a prueba.
 */
namespace Etapas\Etapa3;

/**
 *  DistanciaEdicionClass. Clase desarrollada para la normalización de palabras usando el criterio de distancia de edición. Por ejemplo, la palabra pruebax la normalizara a prueba.
 */
use \Lib\LibInt\MemoriaIntermedioClass;
use \Lib\LibInt\ModuloEtapa3AbstractClass;

class DistanciaEdicionClass extends ModuloEtapa3AbstractClass
{
	
	/**
	* @brief Implementación de método abstracto. El programá principal llamará a este método. Intentará normalizar palabras usando el criterio de distancia de edición.  
	* @return nothing.
	*/

	function ejecutar()
	{
		
		$modulo_name =  substr (get_class($this),strrpos(get_class($this),'\\')+1,strlen(get_class($this)));

		
		$this -> entrar_debug("ejecutar");

		if ($this->salida->existe() == false){
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer(); // Se lee la entrada y se la transforma a minuscula para evitar inconvenientes respecto al case sensitive.
	
		// Se agrega la palabra en minuscula. Ademas se aplica el criterio de sensibilidad a tildes.
		/*foreach($entrada as &$mensajes) {
			foreach($mensajes as &$token_entrada) {
					$token_entrada[5] = mb_strtolower($token_entrada[0],"utf-8");				
			}
		}*/
		
		$h = 0;
		while ($h< count($entrada)){	
			$i = 0;
			while ($i<count($entrada[$h])) {
				$entrada[$h][$i][5] = mb_strtolower($entrada[$h][$i][0],"utf-8");
				$i++;
			}
			$h++;
		}

		$arr0 =  array();	
		//print_r($entrada );
		$i = 0;
		//print_r([$entrada[0][25][0]]);
		//print_r([$entrada[0][26][0]]);
		while ($i<count($entrada)){
		//foreach($entrada as $mensajes) {
			$mensajes = $entrada[$i];
			$j = 0;
			while ($j<count($mensajes)){
			$arr = array();
			//foreach($mensajes2 as $token_entrada) {
				$token_entrada = $mensajes[$j];
				//print_r( $token_entrada[0].$i.$j);
					if ($token_entrada[1] == false and strlen($token_entrada[0])>1) {
						$token_entrada2 = $token_entrada;
						$token_entrada2[6]=array();
						$token_entrada2['distancia1']= $i;
						$token_entrada2['distancia2']= $j;
						array_push($arr,$token_entrada2);
						array_push($arr0,$arr);
					}			
					$j++;
					
			}
			$i++;

		}
		
		if (count($arr0)>0) {
		
				//print_r($arr0);
					
			$obj = new MemoriaIntermedioClass();
			$obj -> inicializar ($arr0,null,$this->debug);

			

			$this -> auxiliar -> set_salida($obj);
			$this -> auxiliar -> set_acent_sensitive(false);
			$this -> auxiliar -> set_levenshtein(true);
			$this -> auxiliar -> set_fonetic(false);
			$this -> auxiliar -> set_lazzy(false);
			$this -> auxiliar -> ejecutar();

			$result = $obj->leer();

			$i = 0;
			while ($i< count($result)){
				foreach($result[$i] as $candidata){		
					if($candidata[1] ==true) {
						//$key = array_search($candidata, $result[$i]);	
						//$key = array_search(mb_strtolower($arr0[$i][$key][0],"utf-8"), array_column($entrada[$i], 5));
						$arr_tmp = array();
						$i2 =$candidata['distancia1'];
						$j2 =$candidata['distancia2'];
						foreach ($candidata[6] as $palabra_candidata) {
							array_push($arr_tmp,array($palabra_candidata,$modulo_name));
						}
						$entrada[$i2][$j2][2] = array_merge($entrada[$i2][$j2][2], $arr_tmp );
						//print_r($entrada);
					}
				}
				$i++;
			}
			$this -> auxiliar -> set_levenshtein(false);
			
			//print_r($entrada_minuscula);
			$this -> salida -> escribir($entrada);
		}
		
		$this -> salir_debug("ejecutar");
	}

}


/*
 * function remove_accent($str) 
{ 
  $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ'); 
  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'); 
  return str_replace($a, $b, $str); 
} 

 */
