<?php

/**
* @file NormalizadorOnomatopeyasClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase NormalizadorOnomatopeyasClass. Clase desarrollada para la normalización de onomatopeyas en Español, tratará sobre todo repeticiones.
 */
namespace Etapas\Etapa3;

/**
 *  NormalizadorOnomatopeyasClass. Clase desarrollada para la normalización de onomatopeyas en Español, tratará sobre todo repeticiones.
 */
use \Lib\LibInt\ArchivoClass;
use \Lib\LibInt\ModuloEtapa3AbstractClass;

class NormalizadorOnomatopeyasClass  extends ModuloEtapa3AbstractClass
{

 	/**
	* @brief Imlementación de método abstracto. El programá principal llamará a este método. Intentará normalizar palabras a onomatopeyas. 
	* @return nothing.
	*/
	function ejecutar()
	{

		$this -> entrar_debug("ejecutar");

		$ruta_base = "./Etapas/Etapa3/NormalizadorOnomatopeyasClass/"; // Es la ruta en la que se encuentran los diccionarios.
		$longitud_maxima = 20; // Es la longitud máxima en cantidad de palabras que forma una onomatopeya.
		
		$arr_acent = array('á','é','í','ó','ú','ä','ë','ï','ö','ü');
		$arr_acent_replace = array('a','e','i','o','u','a','e','i','o','u');
		

		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer(); // Se lee la entrada y se la transforma a minuscula para evitar inconvenientes respecto al case sensitive.

		$list_reglas = array("ReglasOnomatopeyas.reg");

		// Se agrega la palabra en minuscula. Ademas se aplica el criterio de sensibilidad a tildes.
		foreach($entrada as &$mensajes) {
			foreach($mensajes as &$token_entrada) {
					if ($this->acent_sensitive == true) {
						$token_entrada[5] = mb_strtolower($token_entrada[0],"utf-8");
					}
					else{
						$token_entrada[5] = str_replace($arr_acent, $arr_acent_replace,mb_strtolower($token_entrada[0],"utf-8"));
					}
					
					if (!array_key_exists(6,$token_entrada)){
						$token_entrada[6] = array();
					}
			}
		}
	
		// Se recorre cada diccionario. Por cada palabra, se verifica si está dentro del diccionario. Si está dentro del diccionario se la actualiza y se clasifica OOV.
		foreach ($list_reglas as $regla) {
			$archivo_regla = new ArchivoClass();
			$archivo_regla->set_debug($this->debug);
			$archivo_regla->set_ruta($ruta_base.$regla);
			if ($archivo_regla->existe() == false) {
				$this -> warn_debug("ejecutar: Ruta al diccionario no existe.");
			}
			else
			{
				
				
				$list_reglas =  preg_split("/[\n\r]+/",mb_strtolower($archivo_regla -> leer(),"utf-8")); //Separación por salto de línea y espacio en blanco. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.
				foreach($list_reglas as &$regla){
					$arr = array();
					$arr = preg_split("/\|\-\>\|/",mb_strtolower($regla,"utf-8"));
					if ($this->acent_sensitive == true) {
						$arr[0] = str_replace($arr_acent, $arr_acent_replace,$arr[0]);
					}
					$regla = $arr;
				}

				
				
				foreach($entrada as &$mensajes) {			
					$i = 0;
					while ($i<count($mensajes)) {
		
						$j = 0;
						$tmp = "";
						$tmp2 = "";
						
						while ($j<$longitud_maxima and $i+$j< count($mensajes) and  $mensajes[$i+$j][1]==false) {
							if (preg_match("/[[:alnum:]].*/",$mensajes[$i+$j][5])==true) {
								$tmp .= " " . $mensajes[$i+$j][5];
								$tmp2 .= " " . $mensajes[$i+$j][0];
							}
							else {
								$tmp .= $mensajes[$i+$j][5];
								$tmp2 .= $mensajes[$i+$j][0];
							}
							$j++;
						}
						$j--;
						
						$band2 = true;

						while ($band2 and $j>=0){
							$tmp = trim($tmp);			
							$tmp2 = trim($tmp2);	
							
							foreach($list_reglas as &$regla){
								//print_r($regla);
								if ((trim($regla[0])!="" and preg_match($regla[0],$tmp)==true)){					
									$mensajes[$i]['onomatopeyas7'] = $tmp;
									$mensajes[$i]['onomatopeyas8'] = $i;
									$mensajes[$i]['onomatopeyas9'] = $i+$j;
									$mensajes[$i]['onomatopeyas10'] = $regla[1];
									$band2= false;
								}
							}
														
							if ($band2 ==true) {
								$tmp =  trim(substr($tmp,0,strlen($tmp)-strlen($mensajes[$i+$j][0])));
								$tmp2 =  trim(substr($tmp2,0,strlen($tmp2)-strlen($mensajes[$i+$j][0])));			
							}
							$j--;
						}

					$i = $i + $j +2;

					}
				}		
				
		
			}
			$modulo_name =  substr (get_class($this),strrpos(get_class($this),'\\')+1,strlen(get_class($this)));
						//	print_r($list_reglas);
			foreach($entrada as &$mensajes) {
				foreach($mensajes as &$token) {
					if (array_key_exists('onomatopeyas7',$token)) {
			
						$token[0] = $token['onomatopeyas7'];
						//print_r("entro ".$token[0]);
						
						if ($token['onomatopeyas8']<$token['onomatopeyas9']){
							$token[2] = array();
						}
						
						array_push($token[2],array($token['onomatopeyas10'],$modulo_name));
						$count = count($mensajes);
						$i = $token['onomatopeyas8']+1;
					
						while($i<$count and $i<= $token['onomatopeyas9']) {
							unset($mensajes[$i]);
							$i++;
						}
					}			
				}
				$mensajes = array_values($mensajes);
			}

			}
	
		$this -> salida -> escribir($entrada);
		
		$this -> salir_debug("ejecutar");

	}
}
