<?php

/**
* @file NormalizadorHipocoristicosClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase NormalizadorHipocoristicosClass. Clase desarrollada para la normalización de hipocoristicos a nombres propios. 
 */
namespace Etapas\Etapa3;

/**
 *  NormalizadorHipocoristicosClass. Clase desarrollada para la normalización de hipocoristicos a nombres propios. 
 */
use \Lib\LibInt\ArchivoClass;
use \Lib\LibInt\ModuloEtapa3AbstractClass;

class NormalizadorHipocoristicosClass extends ModuloEtapa3AbstractClass
{

 	/**
	* @brief Imlementación de método abstracto. El programá principal llamará a este método. Intentará normalizar palabras de hipocoristicos a nombres propios.   
	* @return nothing.
	*/
	function ejecutar()
	{

		$this -> entrar_debug("ejecutar");

		$ruta_base = "./Etapas/Etapa3/NormalizadorHipocoristicosClass/"; // Es la ruta en la que se encuentran los diccionarios.

		$arr_acent = array('á','é','í','ó','ú','ä','ë','ï','ö','ü');
		$arr_acent_replace = array('a','e','i','o','u','a','e','i','o','u');
		

		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer(); // Se lee la entrada y se la transforma a minuscula para evitar inconvenientes respecto al case sensitive.

		$list_reglas = array("ReglasHipocoristicosFemeninos.reg","ReglasHipocoristicosMasculinos.reg");

		// Se agrega la palabra en minuscula. Ademas se aplica el criterio de sensibilidad a tildes.
		foreach($entrada as &$mensajes) {
			foreach($mensajes as &$token_entrada) {
					if ($this->acent_sensitive == true) {
						$token_entrada[5] = mb_strtolower($token_entrada[0],"utf-8");
					}
					else{
						$token_entrada[5] = str_replace($arr_acent, $arr_acent_replace,mb_strtolower($token_entrada[0],"utf-8"));
					}
					
					if (!array_key_exists(6,$token_entrada)){
						$token_entrada[6] = array();
					}
			}
		}
	
		// Se recorre cada diccionario. Por cada palabra, se verifica si está dentro del diccionario. Si está dentro del diccionario se la actualiza y se clasifica OOV.
		foreach ($list_reglas as $regla) {
			$archivo_regla = new ArchivoClass();
			$archivo_regla->set_debug($this->debug);
			$archivo_regla->set_ruta($ruta_base.$regla);
			if ($archivo_regla->existe() == false) {
				$this -> warn_debug("ejecutar: Ruta al diccionario no existe.");
			}
			else
			{
				
				
				$list_reglas =  preg_split("/[\n\r]+/",mb_strtolower($archivo_regla -> leer(),"utf-8")); //Separación por salto de línea y espacio en blanco. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.
				foreach($list_reglas as &$regla){
					$arr = array();
					$arr = preg_split("/\|\-\>\|/",mb_strtolower($regla,"utf-8"));
					if ($this->acent_sensitive == true) {
						$arr[0] = str_replace($arr_acent, $arr_acent_replace,$arr[0]);
					}
					$regla = $arr;
				}

				
				$i = 0;
				foreach($entrada as &$mensajes) {			
					foreach($mensajes as &$token_entrada) {
						if ($token_entrada[1] == false){
								foreach($list_reglas as &$regla){
									if ((trim($regla[0])!="" and preg_match($regla[0],$token_entrada[5])==true)){		
										//print_r($regla);	
										$modulo_name =  substr (get_class($this),strrpos(get_class($this),'\\')+1,strlen(get_class($this)));					
										array_push($token_entrada[2],array($regla[1],$modulo_name));
									}
									$i++;
							}
							
						}
					}
				}
			}		
		}

	
		$this -> salida -> escribir($entrada);
		
		$this -> salir_debug("ejecutar");

	}
}
