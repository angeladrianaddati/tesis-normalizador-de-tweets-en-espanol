<?php

/**
* @file YuxtaposicionPalabrasClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase YuxtaposiciónPalabrasClass. Clase desarrollada para la normalización de palabras escritas sin separacion de espacio. Tendrá que separar un tokens en varios. Ejemplo: debería resolver problemas caminoacasa devolviendo camino a casa. 
 */

namespace Etapas\Etapa3;

use \Lib\LibInt\MemoriaIntermedioClass;
use \Lib\LibInt\ModuloEtapa3AbstractClass;

/**
 *  YuxtaposiciónPalabrasClass. Clase desarrollada para la normalización de palabras escritas sin separacion de espacio. Tendrá que separar un tokens en varios. Ejemplo: debería resolver problemas caminoacasa devolviendo camino a casa. 
 */
class YuxtaposicionPalabrasClass  extends ModuloEtapa3AbstractClass
{

 	/**
	* @brief Imlementación de método abstracto. El programá principal llamará a este método. Intentará normalizar palabras escritas sin separacion de espacio. Tendrá que separar un tokens en varios. 
	* @return nothing.
	*/
	function ejecutar()
	{
		$modulo_name =  substr (get_class($this),strrpos(get_class($this),'\\')+1,strlen(get_class($this)));


		$this -> entrar_debug("ejecutar");

		$ruta_base = "./Etapas/Etapa3/YuxtaposicionPalabrasClass/"; // Es la ruta en la que se encuentran archivos auxiliares.
		$longitud_maxima = 3; // Es la longitud máxima de palabras que se podrán unir. 
		
		$arr_acent = array('á','é','í','ó','ú','ä','ë','ï','ö','ü');
		$arr_acent_replace = array('a','e','i','o','u','a','e','i','o','u');
		

		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer(); // Se lee la entrada y se la transforma a minuscula para evitar inconvenientes respecto al case sensitive.

		//$list_reglas = array("ReglasOnomatopeyas.reg");


		// Se agrega la palabra en minuscula. Ademas se aplica el criterio de sensibilidad a tildes.
		/*foreach($entrada as &$mensajes) {
			foreach($mensajes as &$token_entrada) {
					if ($this->acent_sensitive == true) {
						$token_entrada[5] = mb_strtolower($token_entrada[0],"utf-8");
					}
					else{
						$token_entrada[5] = str_replace($arr_acent, $arr_acent_replace,mb_strtolower($token_entrada[0],"utf-8"));
					}
					
					if (!array_key_exists(6,$token_entrada)){
						$token_entrada[6] = array();
					}
			}
		}*/
		
		$h = 0;
		while ($h< count($entrada)){	
			$i = 0;
			while ($i<count($entrada[$h])) {
					if ($this->acent_sensitive == true) {
						$entrada[$h][$i][5] = mb_strtolower($entrada[$h][$i][0],"utf-8");
					}
					else{
						$entrada[$h][$i][5] = str_replace($arr_acent, $arr_acent_replace,mb_strtolower($entrada[$h][$i][0],"utf-8"));
					}
					
					if (!array_key_exists(6,$entrada[$h][$i])){
						$entrada[$h][$i][6] = array();
					}
				$i++;
			}
			$h++;
		}
		
	
	
		$h = 0;
		$arr_yuxstaposicion = array();
		
		while ($h< count($entrada)){
			$mensajes = $entrada[$h];			
			$i = 0;
			while ($i<count($mensajes)) {
						
				if ($mensajes[$i][1] == false) {
					$arr = array();
					$j = 2;
					for($j=2; $j<=$longitud_maxima;$j++){
						$arr_tmp = $this->separacion($mensajes[$i][0],$j);
						if (count($arr_tmp)>0){
							$arr= array_merge($arr,$arr_tmp);
						}
					}


					foreach($arr as $palabras){
						$arr0 = array();
						foreach($palabras as $palabra){
							
							$arr1 = array($palabra,false,array(),"","");
							$arr1['yuxtaposicion7'] = $palabra;
							$arr1['yuxtaposicion8'] = $i;
							$arr1['yuxtaposicion9'] = $h;
							$arr1['yuxtaposicion10'] = $palabra;
							array_push($arr0,$arr1); 
						}
						array_push($arr_yuxstaposicion,$arr0);
					}
				}	
			
				$i++;
			}
			$h++;
		}		
		
		if (count($arr_yuxstaposicion)>0) {		
			//print_r($arr_yuxstaposicion);
			//wait(10);
			
			$obj = new MemoriaIntermedioClass();
			$obj -> inicializar ($arr_yuxstaposicion,null,$this->debug);
			$this -> auxiliar -> set_salida($obj);
			$this -> auxiliar -> set_acent_sensitive(false);
			$this -> auxiliar -> set_lazzy(false);

			$this -> auxiliar -> ejecutar();

			$result = $obj->leer();
			
			$i = 0;
			$count0 = count($result);
			
			
			//print_r($arr_yuxstaposicion);
			while ($i<$count0) {
				$candidato = $result[$i];
				$band = true;
				$tmp = "";
				foreach ($candidato as $palabra){
					if ($palabra[1]==false){
						$band = false;
					}
					$tmp .=" ".$palabra[0];			
				}
				if ($band == true){
					//print_r($tmp);
					//print_r($arr_yuxstaposicion[$i]);
					array_push($entrada[$arr_yuxstaposicion[$i][0]['yuxtaposicion9']][$arr_yuxstaposicion[$i][0]['yuxtaposicion8']][2],array($tmp,$modulo_name));
				}
				$i++;
			}

	//print_r($entrada);
			$this -> salida -> escribir($entrada);
		}
		
		$this -> salir_debug("ejecutar");

	}

 	/**
	* @brief Separa una palabra en varias generando todas las posibilidades. Método que llama a uno interno para la recursión.
	* @param $palabra. Palabra.
	* @param $cantidad_palabras_max. Cantidad máxima de palabras que se pueden separar.
	* @return arreglo.
	*/	
	protected function separacion($palabra, $cantidad_palabras_max){
		$arr = $this->separacion_int($palabra, 1, $cantidad_palabras_max);
		if ($arr <> null){
			return $arr;
		}
		else {
			return array();
		}
	}

 	/**
	* @brief Separa una palabra en varias generando todas las posibilidades. Método recursivo.
	* @param $palabra. Palabra.
	* @param $cantidad_palabras_max. Cantidad máxima de palabras que se pueden separar.
	* @return arreglo.
	*/		
	protected function separacion_int($palabra, $cantidad_palabras, $cantidad_palabras_max){
		if (strlen($palabra)<2){
			return null;
		}
		elseif($cantidad_palabras>=$cantidad_palabras_max){
			//print_r($palabra."entro"."\n");
			return array(array($palabra));
		}
		else {
			$i = 2;
			$arr0 = array();
			while ($i<strlen($palabra)) {
				$arr = array();
				$tmp = substr($palabra,0,$i);
				$tmp2 = substr($palabra,$i,strlen($palabra)-1);
				//print_r($tmp."tmp"."\n");
				//print_r($tmp2."tmp2"."\n");
				$arr = $this->separacion_int($tmp2,$cantidad_palabras+1,$cantidad_palabras_max);
				
				$j = 0;
				if ($arr<>null){
				while ($j<count($arr)){
					$palabras = &$arr[$j];
					if ($palabras<>null) {
						$palabras = array_merge(array($tmp),$palabras);
					}
					else {
						unset($arr[$j]);
					}
					$j++;
				}
				$arr = array_values($arr);
				$arr0 = array_merge($arr0,$arr);
			}
				//print_r($arr0);
				$i++;
			}
				
			return $arr0; 
		}
	}
}
