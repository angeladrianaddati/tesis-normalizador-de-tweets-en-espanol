<?php

/**
@file DetectorNumerosClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase DetectorNumerosClass. Clase desarrollada para la detección de numeros y medidas. Se detectan numeros, numeros decimales, numeros escritos con el formato 1.000,00, dinero y porcentajes. Debe trabajar con multipalabaras por lo que tendrá que unir varios tokens.
*/

namespace Etapas\Auxiliar;

/**
 *  DetectorNumerosClass. Clase desarrollada para la detección de numeros y medidas. Se detectan numeros, numeros decimales, numeros escritos con el formato 1.000,00, dinero y porcentajes. Debe trabajar con multipalabaras por lo que tendrá que unir varios tokens.
 */
use \Lib\LibInt\ArchivoClass;
use \Lib\LibInt\ModuloAbstractClass;

class DetectorNumerosClass extends ModuloAbstractClass
{

 	/**
	* @brief Implementación de método abstracto. El programá principal llamará a este método. Realizará la detección de numeros, numeros decimales, numeros escritos con el formato 1.000,00, dinero y porcentajes. Debe trabajar con multipalabaras por lo que tendrá que unir varios tokens. Unirá todos los caracteres consecutivos que podrán formar un número (digito, coma, punto, porcentaje y $).
	* @return nothing.
	*/
	function ejecutar()
	{

		$this -> entrar_debug("ejecutar");

		$ruta_base = "./Etapas/Etapa2/DetectorUrlEmailClass/"; // Es la ruta en la que se encuentran los diccionarios.
		//$longitud_maxima = 30; // Es la longitud máxima de tokens que formarán la url o el mail. Los signos de puntuación cuentan como un token. Por ejemplo http:\\www.google.com tiene una longitud de 9.

		$patter_numero = "/^(\-|\+)?((\d{1,3}(\.\d{3})*)|\d+)(,\d+)?$/";
		$patter_fraccion = "/^(\-|\+)?(((\d{1,3}(\.\d{3})*)|\d+)(,\d+)?)\/(((\d{1,3}(\.\d{3})*)|\d+)(,\d+)?)$/";
		
		
		$patter_moneda = "/^(\-|\+)?(\Sc?)((\d{1,3}(\.\d{3})*)|\d+)(,\d+)?$/";

		$patter_porcentaje = "/^(\-|\+)?((\d{1,3}(\.\d{3})*)|\d+)(,\d+)?\%?$/";
		
		$patter_caracteres_validos = "/^(\-+| \++| \d+| \% | \Sc | \. |, |º |° |\/ )$/";

		$patter_posicion = "/^(\d+(º|°))$/";


		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer(); // Se lee la entrada 

			
		foreach($entrada as &$mensajes) {			
			$i = 0;
			while ($i<count($mensajes)) {
				if (preg_match($patter_numero,$mensajes[$i][0]) or preg_match($patter_fraccion,$mensajes[$i][0]) or preg_match($patter_posicion,$mensajes[$i][0]) or preg_match($patter_moneda,$mensajes[$i][0]) or preg_match($patter_porcentaje,$mensajes[$i][0]))
				{
					$mensajes[$i][1] = true;
				}
				$i++;
			}
		}

		
	$this -> salida -> escribir($entrada);
		
	$this -> salir_debug("ejecutar");
	

	}
}
