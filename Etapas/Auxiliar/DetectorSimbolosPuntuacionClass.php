<?php

/**
@file DetectorSimbolosPuntuacion.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase DetectorEmoticonesClass. Clase desarrollada para la detección de signos de puntuación. Puede requerir unir tokens.
 */
 
namespace Etapas\Auxiliar;

use \Lib\LibInt\ArchivoClass;
use \Lib\LibInt\ModuloAbstractClass;

/**
 *  DetectorEmoticonesClass. Clase desarrollada para la detección de signos de puntuación. Puede requerir unir tokens.
 */
class DetectorSimbolosPuntuacionClass extends ModuloAbstractClass
{

 	/**
	* @brief Imlementación de método abstracto. El programá principal llamará a este método. Realizará la detección de los simbolos de puntuación buscando en diccionarios preestablecidos. Debe unir varios tokens para armar algún signo.
	* @see ejectur().
	* @return nothing.
	*/
	function ejecutar()
	{

		$this -> entrar_debug("ejecutar");

		$ruta_base = __DIR__.DIRECTORY_SEPARATOR.'/DetectorSimbolosPuntuacionClass/'; // Es la ruta en la que se encuentran los diccionarios
		$longitud_maxima = 3; // Es la cantidad maxima de tokens que forman un simbolo. Por ejemplo ... tiene una longitud de 3. 

		$this -> entrar_debug("ejecutar");
		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer(); // Se lee la entrada y se la transforma a minuscula para evitar inconvenientes respecto al case sensitive.
	
		$list_diccionarios = array("DiccionarioSimbolosPuntuacion.dic");

		$list_clasificaciones = array();

		$arr_tmp = array();
		
		// Se recorre cada diccionario. Por cada palabra, se verifica si está dentro del diccionario. Si está dentro del diccionario se la actualiza y se clasifica OOV.
		foreach ($list_diccionarios as $diccionario) {
			$archivo_diccionario = new ArchivoClass();
			$archivo_diccionario->set_debug($this->debug);
			$archivo_diccionario->set_ruta($ruta_base.$diccionario);
			if ($archivo_diccionario->existe() == false) {
				$this -> warn_debug("ejecutar: Ruta al diccionario no existe.");
			}
			else
			{
				
				$list_palabras_diccionario =  preg_split("/[\n\r]+/",mb_strtolower($archivo_diccionario -> leer(),"utf-8")); //Separación por salto de línea. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.
				$list_palabras_diccionario_original =  preg_split("/[\n\r]+/",$archivo_diccionario -> leer());
				$h = 0;
				$arr_tmp = array();
				while ($h<count($entrada)) {
					$mensajes = &$entrada[$h];
						$i = 0;
						$arr_tmp[$h] = array();
					while ($i<count($mensajes)) {
						$band = true;
						$j = 0;
						$tmp = "";
						while ($band and $j<$longitud_maxima and $i+$j< count($mensajes)) {
							//if (strlen($mensajes[$i+$j][0])+$j<$longitud_maxima) {
								$tmp .= $mensajes[$i+$j][0];
							//}
							//else {
							//	$band = false; 
							//}
							$j++;
						}
						$j--;
						
						$band2 = true;
						$k = $j;
						//print_r($tmp."|");
						while ($band2 and $k>=0){
							//print_r($tmp."|");
							if (in_array($tmp,$list_palabras_diccionario) == true) {
								$arr_tmp[$h][$i] = array();
								//print_r($tmp);
								$arr_tmp[$h][$i] [7] = $tmp;
								$arr_tmp[$h][$i] [8] = $i;
								$arr_tmp[$h][$i] [9] = $i+$k;
								$arr_tmp[$h][$i] [-1] = true; 
								$band2= false;
								//$i = $i+$k-2;
							}
							else {
								$tmp = trim(substr($tmp,0,strlen($tmp)-strlen($mensajes[$i+$k][0])));
							}
							$k--;
						}
						$i = $i + $k +2;

					}
					$h++;
				}
			}
			
	
		
			$h = 0;
			while ($h<count($entrada)) {
				$mensajes = &$entrada[$h];
				$i = 0;
				$count1 = count($mensajes);
				while ($i<$count1){
				$token = &$mensajes[$i];
				if (array_key_exists($i,$arr_tmp[$h]))
				{
					$pos = array_search($arr_tmp[$h][$i][7],$list_palabras_diccionario);
					
					$token[0] = $arr_tmp[$h][$i][7];
					$token[1] = true;
					$token[6][] = $list_palabras_diccionario_original[$pos];
					
					$count = count($mensajes);
					$i2 = $arr_tmp[$h][$i][8]+1;
	
					while($i2<=$count1 and $i2<= $arr_tmp[$h][$i][9]) {
						unset($entrada[$h][$i2]);
						unset($arr_tmp[$h][$i2]);
						$i2++;
					}
					$i = $arr_tmp[$h][$i][9];
				}
				$i++;			
			}
			$entrada[$h] = array_values($entrada[$h]);
			$h++;
			
		}
	}
	
	
		$this -> salida -> escribir($entrada);
		
		$this -> salir_debug("ejecutar");
	}

}
