<?php

/**
@file DetectorMetalenguajePlataformaSocialClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase DetectorMetalenguajePlataformaSocialClass. Clase desarrollada para la detección de elementos  del metalenguaje de la red social. Para este caso, solo se detecta el hashtag (#) y la mencion a usuario (@). Debe trabajar con multipalabaras por lo que tendrá que unir varios tokens.
 */
namespace Etapas\Auxiliar;

/**
 *  DetectorMetalenguajePlataformaSocialClass. Clase desarrollada para la detección de elementos  del metalenguaje de la red social. Para este caso, solo se detecta el hashtag (#) y la mencion a usuario (@). Debe trabajar con multipalabaras por lo que tendrá que unir varios tokens.
 */
use \Lib\LibInt\ArchivoClass;
use \Lib\LibInt\ModuloAbstractClass;

class DetectorMetalenguajePlataformaSocialClass extends ModuloAbstractClass
{

 	/**
	* @brief Implementación de método abstracto. El programá principal llamará a este método. Realizará la detección de elementos del metalenguaje de la red social. Para este caso, solo se detecta el hashtag (#) y la mencion a usuario (@). Debe trabajar con multipalabaras por lo que tendrá que unir varios tokens.
	* @return nothing.
	*/
	function ejecutar()
	{

		$this -> entrar_debug("ejecutar");

		$ruta_base = "./Etapas/Etapa2/DetectorMetalenguajePlataformaSocialClass/"; // Es la ruta en la que se encuentran los diccionarios.
		$longitud_maxima = 2; // Es la longitud máxima de tokens que formarán la url o el mail. Los signos de puntuación cuentan como un token. Por ejemplo http:\\www.google.com tiene una longitud de 9.

		$patter_tweet_user = "/^((Vía\s){0,1}\@([[:alnum:]]|[á-źÁ-ŹäëïöüÄËÏÖÜ]|\_|ñ|Ñ|\-)+)$/i";
		
		$patter_tweet_hashtag = "/^(\#([[:alnum:]]|[á-źÁ-ŹäëïöüÄËÏÖÜ]|\_|ñ|Ñ|\-)+)$/i";
		
		$patter_tweet = "/^((RT)|(\+10*)|(MT)|(PRT)|(TT)|(TL)|(CC)|(EN)|(FA)|(FF)|(NSFW)|(PDF)|(♺)|(RTRL))$/i";
			
		
		$patter_caracteres_validos = "/^(([[:alnum:]]|[á-źÁ-ŹäëïöüÄËÏÖÜ])+| \# | \@ | \s | \+ | \♺ |\_ |\-|ñ|Ñ )$/";
		
		

		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer(); // Se lee la entrada 

			
		foreach($entrada as &$mensajes) {			
			$i = 0;
			while ($i<count($mensajes)) {		
				if (preg_match($patter_tweet_user,$mensajes[$i][0]) or preg_match($patter_tweet_hashtag,$mensajes[$i][0]) or preg_match($patter_tweet,$mensajes[$i][0])){
					//echo $mensajes[$i][0]."matcho!!!";
					$mensajes[$i][1] = true;
				}

				$i ++;
		
			}
		}


	$this -> salida -> escribir($entrada);
		
	$this -> salir_debug("ejecutar");
	

	}
}
