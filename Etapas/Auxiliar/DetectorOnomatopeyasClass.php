<?php

/**
@file DetectorOnomatopeyasClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase DetectorOnomatopeyasClass. Clase desarrollada para la detección de onomatopeyas en español. Debe trabajar con multipalabaras por lo que tendrá que unir varios tokens.
 */

namespace Etapas\Auxiliar;

/**
 *  DetectorOnomatopeyasClass. Clase desarrollada para la detección de onomatopeyas en español. Debe trabajar con multipalabaras por lo que tendrá que unir varios tokens.
 */
use \Lib\LibInt\ArchivoClass;
use \Lib\LibInt\ModuloAbstractClass;

class DetectorOnomatopeyasClass extends ModuloAbstractClass
{

 	/**
	* @brief Implementación de método abstracto. El programá principal llamará a este método. Realizará la detección de onomatopeyas buscando en diccionarios preestablecidos. Debe trabajar con multipalabaras por lo que tendrá que unir varios tokens.
	* @return nothing.
	*/
	function ejecutar()
	{
		$this -> entrar_debug("ejecutar");

		$ruta_base =  __DIR__.DIRECTORY_SEPARATOR.'DetectorOnomatopeyasClass/'; // Es la ruta en la que se encuentran los diccionarios.
		$longitud_maxima = 10; // Es la longitud máxima en cantidad de palabras que forma una entidad nombrada. Los signos de puntuación cuentan como una palabras. Por ejemplo S.A. tiene una longitud de 4.
		
		$arr_acent = array('á','é','í','ó','ú','ä','ë','ï','ö','ü');
		$arr_acent_replace = array('a','e','i','o','u','a','e','i','o','u');
		

		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer(); // Se lee la entrada y se la transforma a minuscula para evitar inconvenientes respecto al case sensitive.

		$list_diccionarios = array("DiccionarioOnomatopeyas.dic");

		$list_clasificaciones = array();

		// Se agrega la palabra en minuscula. Ademas se aplica el criterio de sensibilidad a tildes.
		foreach($entrada as &$mensajes) {
			foreach($mensajes as &$token_entrada) {
					if ($this->acent_sensitive == true) {
						$token_entrada[5] = mb_strtolower($token_entrada[0],"utf-8");
					}
					else{
						$token_entrada[5] = str_replace($arr_acent, $arr_acent_replace,mb_strtolower($token_entrada[0],"utf-8"));
					}
					
					if (!array_key_exists(6,$token_entrada)){
						$token_entrada[6] = array();
					}
			}
		}
	
	
		// Se recorre cada diccionario. Por cada palabra, se verifica si está dentro del diccionario. Si está dentro del diccionario se la actualiza y se clasifica OOV.
		foreach ($list_diccionarios as $diccionario) {
			$archivo_diccionario = new ArchivoClass();
			$archivo_diccionario->set_debug($this->debug);
			$archivo_diccionario->set_ruta($ruta_base.$diccionario);
			if ($archivo_diccionario->existe() == false) {
				$this -> warn_debug("ejecutar: Ruta al diccionario no existe.");
			}
			else
			{
				
				if ($this->acent_sensitive == true) {
					$list_palabras_diccionario =  preg_split("/[\n\r]+/",mb_strtolower($archivo_diccionario -> leer(),"utf-8")); //Separación por salto de línea y espacio en blanco. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.
					$list_palabras_diccionario_original = $list_palabras_diccionario;
				}
				else {
					$str = mb_strtolower($archivo_diccionario -> leer(),"utf-8");
					$list_palabras_diccionario =  preg_split("/[\n\r]+/",str_replace($arr_acent, $arr_acent_replace,$str)); //Separación por salto de línea y espacio en blanco. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.
					$list_palabras_diccionario_original = preg_split("/[\n\r]+/",$str);

				}
				
				//$list_palabras_diccionario =  preg_split("/[\n\r]+/",mb_strtolower($archivo_diccionario -> leer(),"utf-8")); //Separación por salto de línea. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.


			
		// Se agrega la palabra en minuscula. Ademas se aplica el criterio de sensibilidad a tildes.
				$h = 0;
				$arr_tmp = array();
				while ($h<count($entrada)) {
					$mensajes = &$entrada[$h];
					$i = 0;
					$arr_tmp[$h] = array();
					while ($i<count($mensajes)) {
		
						$j = 0;
						$tmp = "";
						$tmp2 = "";
						
						while ($j<$longitud_maxima and $i+$j< count($mensajes)) {
							if (preg_match("/[[:alnum:]].*/",$mensajes[$i+$j][5])==true) {
								$tmp .= " " . $mensajes[$i+$j][5];
								$tmp2 .= " " . $mensajes[$i+$j][0];
							}
							else {
								$tmp .= $mensajes[$i+$j][5];
								$tmp2 .= $mensajes[$i+$j][0];
							}
							$j++;
						}
						$j--;
						
						$band2 = true;

						while ($band2 and $j>=0){
							$tmp = trim($tmp);			
							$tmp2 = trim($tmp2);	

							if (in_array($tmp,$list_palabras_diccionario) == true) {
								$arr_tmp[$h][$i] = array();
								//print_r($tmp);
								$arr_tmp[$h][$i] [7] = $tmp2;
								$arr_tmp[$h][$i] [8] = $i;
								$arr_tmp[$h][$i] [9] = $i+$j;
								$arr_tmp[$h][$i] ['DetectorOnomatopeyas'] = true; 
									$band2= false;
								}
							else {
								$tmp =  trim(substr($tmp,0,strlen($tmp)-strlen($mensajes[$i+$j][0])));
								$tmp2 =  trim(substr($tmp2,0,strlen($tmp2)-strlen($mensajes[$i+$j][0])));
							}
							
							$j--;
						}


					$i = $i + $j +2;
					
					}
					$h++;

					}
				}					
				
		
			}
			

			
			//print_r($list_palabras_diccionario);
			//print_r($list_palabras_diccionario_original);
			//print_r($entrada );
			$h = 0;
			while ($h<count($entrada)) {
				$mensajes = &$entrada[$h];
				$i = 0;
				$count1 = count($mensajes);
				while ($i<$count1){
				$token = &$mensajes[$i];
				if (array_key_exists($i,$arr_tmp[$h]))
				{
					$pos = array_search($arr_tmp[$h][$i][7],$list_palabras_diccionario);
					
					$token[0] = $arr_tmp[$h][$i][7];
					$token[1] = true;
					$token[6] = $list_palabras_diccionario_original[$pos];
					
					$count = count($mensajes);
					$fin = $arr_tmp[$h][$i][9];
					$i2 = $arr_tmp[$h][$i][8]+1;
	
					while($i2<=$count1 and $i2<= $fin) {
						unset($entrada[$h][$i2]);
						unset($arr_tmp[$h][$i2]);
						$i2++;
					}
					$i = $i2--;
				}
				$i++;			
			}
			$entrada[$h] = array_values($entrada[$h]);
			$h++;
			
		}
		
		
		$this -> salida -> escribir($entrada);
		
		$this -> salir_debug("ejecutar");

	}
}
