<?php

/**
@file DetectorPalabra2Class.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase DetectorPalabra2Class. Clase desarrollada para la detección de palabras dentro del diccionario de mejora continua.
 */
namespace Etapas\Auxiliar;

use \Lib\LibInt\ArchivoClass;
use \Lib\LibInt\ModuloAbstractClass;


/**
 *  DetectorPalabra2Class. Clase desarrollada para la detección de palabras dentro del diccionario de mejora continua.
 */
class DetectorPalabras2Class extends ModuloAbstractClass{
		protected	$ruta_base =  __DIR__.DIRECTORY_SEPARATOR.'/DetectorPalabras2Class/'; // Es la ruta en la que se encuentran los diccionarios
		protected	$list_diccionarios = array("DiccionarioMejoraContinua.dic");
		protected	$diccionarios = array();
		protected	$diccionarios_original = array();
		protected	$diccionarios_acent = array();

 	/**
	* Implementación de método abstracto.  El programá principal llamará a este método. Realizará la detección de las palabras buscando en diccionario de mejora continua.
	* @return nothing.
	*/
	function ejecutar()
	{

		$this -> entrar_debug("ejecutar");
	
		$arr_acent = array('á','é','í','ó','ú','ä','ë','ï','ö','ü');
		$arr_acent_replace = array('a','e','i','o','u','a','e','i','o','u');


		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada = $this -> salida -> leer();
		
		if (count($this->diccionarios)==0){
			$this->inicializar_diccionarios();
		}


		// Se agrega la palabra en minuscula. Ademas se aplica el criterio de sensibilidad a tildes.
		foreach($entrada as &$mensajes) {
			foreach($mensajes as &$token_entrada) {
					if ($this->acent_sensitive == true) {
						$token_entrada[5] = mb_strtolower($token_entrada[0],"utf-8");
					}
					else{
						$token_entrada[5] = str_replace($arr_acent, $arr_acent_replace,mb_strtolower($token_entrada[0],"utf-8"));
					}
					
					if (!array_key_exists(6,$token_entrada)){
						$token_entrada[6] = array();
					}
			}
		}
		
		$i = 0;
		while ($i<count($this->diccionarios)){
			
			if($this->acent_sensitive==true) 
				$list_palabras_diccionario = $this->diccionarios[$i];
			else 
				$list_palabras_diccionario = $this->diccionarios_acent[$i];
			
			$list_palabras_diccionario_original = $this->diccionarios_original[$i];
			
			if (count($list_palabras_diccionario)>0){
				//echo "intermedio: ".$diccionario." ".count($list_palabras_diccionario);
	
				//$cantidad = count($list_palabras_diccionario);
				//echo "por entraaar".$this->lazzy ;
			if ($this->lazzy == true){
					$list_palabras_diccionario2 = $list_palabras_diccionario;
				//echo "entroooooo";
					foreach($list_palabras_diccionario2 as &$palabra){
					$caracteres = str_split($palabra);
					asort($caracteres);
					$palabra = implode("",$caracteres); 
					}

								
				//$cantidad = count($list_palabras_diccionario);
				//echo "por entrar al bucle";
				foreach($entrada as &$mensajes) {
					foreach ($mensajes as &$token_entrada) {
							$caracteres = str_split($token_entrada[5]);
							asort($caracteres);
							$caracteres = implode("",$caracteres);
							$keys = array_keys($list_palabras_diccionario2, $caracteres);
							
							//print_r($keys);
							foreach($keys as $key){
								//echo $token_entrada[6];
								$token_entrada[1] = true;
								array_push($token_entrada[6],$list_palabras_diccionario_original[$key]);
								//break;					
							}

					}
				}
			}
			elseif ($this->levenshtein==true) {
				foreach($entrada as &$mensajes) {
					foreach ($mensajes as &$token_entrada) {
						for ($j=0; $j<count($list_palabras_diccionario); $j++){
							if (levenshtein($list_palabras_diccionario[$j],$token_entrada[5])<2){
								//echo $token_entrada[5]; echo $j;
								$token_entrada[1] = true;
								array_push($token_entrada[6],$list_palabras_diccionario_original[$j]);
								//break;
							}
							
						}
						$token_entrada[6] = array_unique($token_entrada[6]);

					}
				}
			}
			elseif ($this->fonetic == true){

					$list_palabras_diccionario2 = $list_palabras_diccionario;
				//echo "entroooooo";
					foreach($list_palabras_diccionario2 as &$palabra){
					$tmp = spanish_metaphone($palabra );
					if (strlen($tmp)>0)
						$palabra = $tmp ;
					}


								
				//$cantidad = count($list_palabras_diccionario);
				//echo "por entrar al bucle";
				foreach($entrada as &$mensajes) {
					foreach ($mensajes as &$token_entrada) {
						
							$keys = array_keys($list_palabras_diccionario2, spanish_metaphone($token_entrada[5]));
							
							//print_r($keys);
							foreach($keys as $key){
								//echo $token_entrada[6];
								$token_entrada[1] = true;
								array_push($token_entrada[6],$list_palabras_diccionario_original[$key]);
								//break;					
							}
							
							$token_entrada[6] = array_unique($token_entrada[6]);

					}
				}
			}
				//echo "fin";
		else{
						
			//echo "noooo entroooooo";
			foreach($entrada as &$mensajes) {
				foreach ($mensajes as &$token_entrada) {
					$pos = array_search($token_entrada[5],$list_palabras_diccionario,TRUE);
					//$pos = $this->busqueda_dicotomica($list_palabras_diccionario,0,$cantidad,$token_entrada[5]);
					//asort($palabras);	
					if ($pos !== false) {
						$token_entrada[1] = true;
						array_push($token_entrada[6],$list_palabras_diccionario_original[$pos]);
						//break;
					}
				}
			}
		}
					$i++;
		}
	}
		$this -> salida -> escribir($entrada);	
		$this -> salir_debug("ejecutar");
		
	}

public function inicializar_diccionarios(){
		$arr_acent = array('á','é','í','ó','ú','ä','ë','ï','ö','ü');
		$arr_acent_replace = array('a','e','i','o','u','a','e','i','o','u');
		
		// Se recorre cada diccionario. Por cada palabra, se verifica si está dentro del diccionario. Si está dentro del diccionario se la actualiza y se clasifica OOV.
		foreach ($this->list_diccionarios as $dic) {
			$archivo_diccionario = new ArchivoClass();
			$archivo_diccionario->set_debug($this->debug);
			$archivo_diccionario->set_ruta($this -> ruta_base.$dic);
			if ($archivo_diccionario->existe() == false) {
				$this -> warn_debug("ejecutar: Ruta al diccionario no existe.");
			}
			else
			{
				//echo "inicio";
		//		if ($this->acent_sensitive == true) {
					$str = mb_strtolower($archivo_diccionario -> leer(),"utf-8");
					$list_palabras_diccionario =  preg_split("/[\s]+/",$str); //Separación por salto de línea y espacio en blanco. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.
					$list_palabras_diccionario_original = $list_palabras_diccionario;
		//		}
		//		else {
					
					$list_palabras_diccionario_acent =  preg_split("/[\s]+/",str_replace($arr_acent, $arr_acent_replace,$str)); //Separación por salto de línea y espacio en blanco. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.
					//$list_palabras_diccionario_original = preg_split("/[\s]+/",$str);

		//		}
		
				$list_palabras_diccionario = array_values(array_diff($list_palabras_diccionario , array('')));
				$list_palabras_diccionario_original = array_values(array_diff($list_palabras_diccionario_original , array('')));
				$list_palabras_diccionario_acent = array_values(array_diff($list_palabras_diccionario_acent , array('')));
				
				$this->diccionarios[] = $list_palabras_diccionario;
				$this->diccionarios_original[] = $list_palabras_diccionario_original;
				$this->diccionarios_acent[] = $list_palabras_diccionario_acent;
			}
		}				
	
}
   

}


