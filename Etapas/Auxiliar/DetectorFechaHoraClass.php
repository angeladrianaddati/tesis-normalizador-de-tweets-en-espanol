<?php

/**
@file DetectorFechaHoraClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase DetectorFechaHoraClass. Clase desarrollada para la detección de fechas y horas. 
 */

namespace Etapas\Auxiliar;

use \Lib\LibInt\ArchivoClass;
use \Lib\LibInt\ModuloAbstractClass;

/**
 *  DetectorFechaHoraClass. Clase desarrollada para la detección de fechas y horas. 
 */
class DetectorFechaHoraClass extends ModuloAbstractClass
{

 	/**
	* @brief Imlementación de método abstracto. El programá principal llamará a este método. Realizará la detección de fechas y horas. Debe trabajar con multipalabaras por lo que tendrá que unir varios tokens. Unirá todos los caracteres consecutivos que podrán formar una fecha (digito, coma, punto).
	* @return nothing.
	*/
	function ejecutar()
	{

		$this -> entrar_debug("ejecutar");

		$ruta_base = "./Etapas/Etapa2/DetectorUrlEmailClass/"; // Es la ruta en la que se encuentran los diccionarios.
		$longitud_maxima = 7; // Es la longitud máxima de tokens que formarán la url o el mail. Los signos de puntuación cuentan como un token. Por ejemplo http:\\www.google.com tiene una longitud de 9.

		$patter_hora = "/^((\d{1,2}(\:\d\d){1,2})|(\d{1,2}((\:\d\d){1,2}){0,1}))(hs)?$/";
		
		$patter_fecha = "/^(\d{1,2})((\.|\/|\-)\d{1,2})?((\.|\/|\-)\d{1,2}|(\.|\/|\-)\d{4,4})$/";

		$patter_caracteres_validos = "/^(\d+|\/|\:|\.|\-|h|s)$/";


		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer(); // Se lee la entrada 

			
		foreach($entrada as &$mensajes) {			
			$i = 0;
			while ($i<count($mensajes)) {
				
				if (preg_match($patter_fecha,$mensajes[$i][0]) or preg_match($patter_hora,$mensajes[$i][0]))
				{
					$mensajes[$i][1] = true;
				}

				$i++;
		
			}
		}
	
	$this -> salida -> escribir($entrada);
		
	$this -> salir_debug("ejecutar");
	

	}
}
