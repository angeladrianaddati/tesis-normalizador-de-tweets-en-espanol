<?php

/**
@file DetectorEntidadesNombradasClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase DetectorEntidadesNombradasClass. Clase desarrollada para la detección de entidades nombradas. Debe trabajar con multipalabaras por lo que tendrá que unir varios tokens.
 */

namespace Etapas\Auxiliar;


use \Lib\LibInt\ArchivoClass;
use \Lib\LibInt\ModuloAbstractClass;


/**
 *  DetectorEntidadesNombradasClass. Clase desarrollada para la detección de entidades nombradas. Debe trabajar con multipalabaras por lo que tendrá que unir varios tokens.
 */
 
class DetectorEntidadesNombradasClass extends ModuloAbstractClass
{
		protected	$ruta_base =  __DIR__.DIRECTORY_SEPARATOR.'DetectorEntidadesNombradasClass/'; // Es la ruta en la que se encuentran los diccionarios.
		protected	$list_diccionarios = array("DiccionarioEstados.dic","DiccionarioPaises.dic","DiccionarioContinentes.dic","DiccionarioNombres.dic", "DiccionarioApellidos.dic","DiccionarioEntidadesNombradasPersonalizado.dic"); // Listado de diccionarios a leer.
		protected	$diccionarios = array();
		protected	$diccionarios_original = array();
		protected	$diccionarios_acent = array();

 	/**
	* @brief Implementación de método abstracto. El programá principal llamará a este método. Realizará la detección de entidades nombradas buscando en diccionarios preestablecidos. Debe trabajar con multipalabaras por lo que tendrá que unir varios tokens.
	* @return nothing.
	*/
	function ejecutar()
	{
		$this -> entrar_debug("ejecutar");

		
		$longitud_maxima = 10; // Es la longitud máxima en cantidad de palabras que forma una entidad nombrada. Los signos de puntuación cuentan como una palabras. Por ejemplo S.A. tiene una longitud de 4.
		
		$arr_acent = array('á','é','í','ó','ú','ä','ë','ï','ö','ü');
		$arr_acent_replace = array('a','e','i','o','u','a','e','i','o','u');
		

		if ($this->salida->existe() == false)
		{
			$this -> warn_debug("ejecutar: Ruta al archivo intermedio no existe.");
			exit("");
		}

		$entrada =$this -> salida -> leer(); // Se lee la entrada y se la transforma a minuscula para evitar inconvenientes respecto al case sensitive.

		if (count($this->diccionarios)==0){
			$this->inicializar_diccionarios();
		}


		// Se agrega la palabra en minuscula. Ademas se aplica el criterio de sensibilidad a tildes.
		foreach($entrada as &$mensajes) {
			foreach($mensajes as &$token_entrada) {
					if ($this->acent_sensitive == true) {
						$token_entrada[5] = mb_strtolower($token_entrada[0],"utf-8");
					}
					else{
						$token_entrada[5] = str_replace($arr_acent, $arr_acent_replace,mb_strtolower($token_entrada[0],"utf-8"));
					}
					
					if (!array_key_exists(6,$token_entrada)){
						$token_entrada[6] = array();
					}
			}
		}
		

		$i0 = 0;
		while ($i0<count($this->diccionarios)){
			$arr_tmp = array();
			if($this->acent_sensitive==true) 
				$list_palabras_diccionario = $this->diccionarios[$i0];
			else 
				$list_palabras_diccionario = $this->diccionarios_acent[$i0];
			
			$list_palabras_diccionario_original = $this->diccionarios_original[$i0];
				
				//$list_palabras_diccionario =  preg_split("/[\n\r]+/",mb_strtolower($archivo_diccionario -> leer(),"utf-8")); //Separación por salto de línea. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.

		// Se agrega la palabra en minuscula. Ademas se aplica el criterio de sensibilidad a tildes.
				$h = 0;
				while ($h<count($entrada)){	
					$mensajes = &$entrada[$h];	
					$arr_tmp[$h] = array();	
					$i = 0;
					while ($i<count($mensajes)) {
						
						if ($this->lazzy == false and $this->levenshtein == false and $this->fonetic == false){

						$j = 0;
						$tmp = "";
						$tmp2 = "";
						
						while ($j<$longitud_maxima and $i+$j< count($mensajes)) {
							if (preg_match("/([[:alnum:]]|[á-źÁ-ŹäëïöüÄËÏÖÜ]).*/",$mensajes[$i+$j][5])==true) {
								$tmp .= " " . $mensajes[$i+$j][5];
								$tmp2 .= " " . $mensajes[$i+$j][0];
							}
							else {
								$tmp .= $mensajes[$i+$j][5];
								$tmp2 .= $mensajes[$i+$j][0];
							}
							$j++;
						}
						$j--;
						
						$band2 = true;

						while ($band2 and $j>=0){
							$tmp = trim($tmp);			
							$tmp2 = trim($tmp2);	
							//print_r('Entrando'.$tmp2);
							if (in_array($tmp,$list_palabras_diccionario) == true) {
									$arr_tmp[$h][$i] = array();
									$arr_tmp[$h][$i][6] = $tmp;
									$arr_tmp[$h][$i][7] = $tmp2;
									$arr_tmp[$h][$i][8] = $i;
									$arr_tmp[$h][$i][9] = $i+$j;
									//$arr_tmp[$h][$i][-1] = true;
									/*
									$mensajes[$i][7] = $tmp2;
									$mensajes[$i][8] = $i;
									$mensajes[$i][9] = $i+$j;
									$mensajes[$i][-1] = true;
									*/
									$band2= false;
								}
							else {
								$tmp =  trim(substr($tmp,0,strlen($tmp)-strlen($mensajes[$i+$j][0])));
								$tmp2 =  trim(substr($tmp2,0,strlen($tmp2)-strlen($mensajes[$i+$j][0])));			
							}
							$j--;
						}

					$i = $i + $j +2;

					}
					else {
							$arr_tmp[$h][$i] = array();
							$arr_tmp[$h][$i][6] = $mensajes[$i][5];
							$arr_tmp[$h][$i][7] = $mensajes[$i][0];
							$arr_tmp[$h][$i][8] = $i;
							$arr_tmp[$h][$i][9] = $i;
							$i++;
					}
				}
					$h++;
				
			}					
				
				$i0++;

			//print_r($arr_tmp);	
			//exit(1);
			if ($this->lazzy == true){
					$list_palabras_diccionario2 = $list_palabras_diccionario;
				//echo "entroooooo";
					foreach($list_palabras_diccionario2 as &$palabra){
					$caracteres = str_split($palabra);
					asort($caracteres);
					$palabra = implode("",$caracteres); 
					}

				$h = 0;
				while ($h<count($entrada)) {
					$mensajes = &$entrada[$h];
					$i = 0;
					$count1 = count($mensajes);
					while ($i<$count1){
					$token = &$mensajes[$i];
					if (array_key_exists($i,$arr_tmp[$h]))
					{
						
						$caracteres = str_split($arr_tmp[$h][$i][6]);
						asort($caracteres);
						$caracteres = implode("",$caracteres);
						$keys = array_keys($list_palabras_diccionario2, $caracteres);
						
						if (count($keys)>0 and $arr_tmp[$h][$i][8]!=$arr_tmp[$h][$i][9]){
							$token[6] = array();
						}
						//print_r($keys);
						foreach($keys as $key){
							//echo $token_entrada[6];
							$token[0] = $arr_tmp[$h][$i][7];
							$token[1] = true;
							$token[6][] = $list_palabras_diccionario_original[$key];
							//$token_entrada[1] = true;
							//array_push($token_entrada[6],$list_palabras_diccionario_original[$key]);
							//break;					
						}
							
						$count = count($mensajes);
						$i2 = $arr_tmp[$h][$i][8]+1;
		
						while($i2<=$count1 and $i2<= $arr_tmp[$h][$i][9]) {
							unset($entrada[$h][$i2]);
							unset($arr_tmp[$h][$i2]);
							$i2++;
						}
						$i = $arr_tmp[$h][$i][9];
					}
					$i++;			
				}
				$entrada[$h] = array_values($entrada[$h]);
				$h++;
				}
						
				
			}
			elseif ($this->levenshtein==true) {
				
				$h = 0;
				while ($h<count($entrada)) {
					$mensajes = &$entrada[$h];
					$i = 0;
					$count1 = count($mensajes);
					while ($i<$count1){
						$token = &$mensajes[$i];
						for ($j=0; $j<count($list_palabras_diccionario); $j++){
							if (levenshtein($list_palabras_diccionario[$j],$token_entrada[5])<2){
								//print_r($token);
								//echo "palabra";print_r($arr_tmp[$h]);
								//echo "palabra2";
								$token[0] = $arr_tmp[$h][$i][7];
								$token[1] = true;
								$token[6][] = $list_palabras_diccionario_original[$j];
							
								$count = count($mensajes);
								$i2 = $arr_tmp[$h][$i][8]+1;
				
								while($i2<=$count1 and $i2<= $arr_tmp[$h][$i][9]) {
									unset($entrada[$h][$i2]);
									unset($arr_tmp[$h][$i2]);
									$i2++;
								}
								$i = $arr_tmp[$h][$i][9];
							}
						}
						$token_entrada[6] = array_unique($token_entrada[6]);
						$i++;			
				}
				$entrada[$h] = array_values($entrada[$h]);
				$h++;
				}
			}
			elseif ($this->fonetic == true){
				
				$list_palabras_diccionario2 = $list_palabras_diccionario;
				//echo "entroooooo";
				foreach($list_palabras_diccionario2 as &$palabra){
					$tmp = spanish_metaphone($palabra );
					if (strlen($tmp)>0)
						$palabra = $tmp ;
				}
				
				$h = 0;
				while ($h<count($entrada)) {
					$mensajes = &$entrada[$h];
					$i = 0;
					$count1 = count($mensajes);
					while ($i<$count1){
					$token = &$mensajes[$i];
					if (array_key_exists($i,$arr_tmp[$h]))
					{
						
						$keys = array_keys($list_palabras_diccionario2, spanish_metaphone($arr_tmp[$h][$i][6]));
						
						if (count($keys)>0 and $arr_tmp[$h][$i][8]!=$arr_tmp[$h][$i][9]){
							$token[6] = array();
						}
						//print_r($keys);
						foreach($keys as $key){
							//echo $token_entrada[6];
							$token[0] = $arr_tmp[$h][$i][7];
							$token[1] = true;
							$token[6][] = $list_palabras_diccionario_original[$key];
							//$token_entrada[1] = true;
							//array_push($token_entrada[6],$list_palabras_diccionario_original[$key]);
							//break;					
						}
							
						$count = count($mensajes);
						$i2 = $arr_tmp[$h][$i][8]+1;
		
						while($i2<=$count1 and $i2<= $arr_tmp[$h][$i][9]) {
							unset($entrada[$h][$i2]);
							unset($arr_tmp[$h][$i2]);
							$i2++;
						}
						$i = $arr_tmp[$h][$i][9];
					}
					$i++;			
				}
				$entrada[$h] = array_values($entrada[$h]);
				$h++;
				}
						
				
			} 
			else{
				$h = 0;
				while ($h<count($entrada)) {
					$mensajes = &$entrada[$h];
					$i = 0;
					$count1 = count($mensajes);
					while ($i<$count1){
					$token = &$mensajes[$i];
					if (array_key_exists($i,$arr_tmp[$h]))
					{
						$pos = array_search($arr_tmp[$h][$i][6],$list_palabras_diccionario);
						
						//print_r($arr_tmp);
						
						$token[0] = $arr_tmp[$h][$i][7];
						$token[1] = true;
						$token[6][] = $list_palabras_diccionario_original[$pos];
					
						$count = count($mensajes);
						$i2 = $arr_tmp[$h][$i][8]+1;
		
						while($i2<=$count1 and $i2<= $arr_tmp[$h][$i][9]) {
							unset($entrada[$h][$i2]);
							unset($arr_tmp[$h][$i2]);
							$i2++;
						}
						$i = $arr_tmp[$h][$i][9];
					}
					$i++;			
				}
				$entrada[$h] = array_values($entrada[$h]);
				$h++;
				}
			}

	}
		//print_r($entrada);
		
		//print_r($entrada);
		
		
		$this -> salida -> escribir($entrada);
		//wait();
		
		$this -> salir_debug("ejecutar");

	}
	

 	/**
	* @brief Método interno que lee las palabras de los diccionarios.
	* @return nothing.
	*/
	protected function inicializar_diccionarios(){
		$arr_acent = array('á','é','í','ó','ú','ä','ë','ï','ö','ü');
		$arr_acent_replace = array('a','e','i','o','u','a','e','i','o','u');
		
		// Se recorre cada diccionario. Por cada palabra, se verifica si está dentro del diccionario. Si está dentro del diccionario se la actualiza y se clasifica OOV.
		foreach ($this->list_diccionarios as $dic) {
			$archivo_diccionario = new ArchivoClass();
			$archivo_diccionario->set_debug($this->debug);
			$archivo_diccionario->set_ruta($this -> ruta_base.$dic);
			if ($archivo_diccionario->existe() == false) {
				$this -> warn_debug("ejecutar: Ruta al diccionario no existe.");
			}
			else
			{
				//echo "inicio";
		//		if ($this->acent_sensitive == true) {
					$str = mb_strtolower($archivo_diccionario -> leer(),"utf-8");
					$list_palabras_diccionario =  preg_split("/[\n\r]+/",$str); //Separación por salto de línea y espacio en blanco. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.
					$list_palabras_diccionario_original = $list_palabras_diccionario;
		//		}
		//		else {
					
					$list_palabras_diccionario_acent =  preg_split("/[\n\r]+/",str_replace($arr_acent, $arr_acent_replace,$str)); //Separación por salto de línea y espacio en blanco. Se pasa la palabra del diccionario a minuscula para evitar inconvenientes.
					//$list_palabras_diccionario_original = preg_split("/[\s]+/",$str);

		//		}
				$this->diccionarios[] = $list_palabras_diccionario;
				$this->diccionarios_original[] = $list_palabras_diccionario_original;
				$this->diccionarios_acent[] = $list_palabras_diccionario_acent;
			}
		}				
	
}

}
