<?php 

/**
* @file main_metricas.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Programa Secundario: Programa creado para realizar el cálculos de las métricas de Cobertura, Presición y cobertura..
* 
*/

include("./autoloader.php");

use \Lib\LibInt\DebugClass;
use \Lib\LibInt\ArchivoIntermedioClass;
use \Lib\LibInt\ArchivoConfiguracionClass;
use \Lib\LibInt\ArchivoMemoriaClass;
use \Lib\LibInt\EtapaClass;
use \Lib\LibInt\Etapa3Class;
use \Lib\LibInt\Etapa4Class;
use \Etapas\Etapa3\YuxtaposicionPalabrasClass;
/* ========== ENCABEZADO DE DECLARACIONES ========== */

/* ========== RUTAS A LOS ARCHIVOS ========== */
$log = "./log.txt";
//$archivo_entrada = "./Entrada/prueba_fase01_4.xml";
//$archivo_salida = "./Salida/prueba_fase01_4_v2.xml";
//$archivo_intermedio = "./Intermedio/intermedio_validacion.xml";
//$archivo_entrada = "./Etapas/Etapa0/inicializar_03_1.xml";
//$archivo_entrada = "./Entrada/entrada_validacion.xml";

//$archivo_salida = "./Salida/salida_validacion.xml";
//$archivo_salida = "./Salida/salida.xml";

//$archivo_intermedio = "./Intermedio/intermedio_inicializar_02.xml";
/*
$archivo_entrada = "./Entrada/entrada_caso_estudio.xml";
$archivo_salida = "./Salida/salida_caso_estudio.xml";
$archivo_intermedio = "./Intermedio/intermedio_caso_estudio.xml";*/

$ruta_etiquetado = "./intermedio_validacion_etiquetado.xml";
//$ruta_ejecucion = "./intermedio_validacion.xml";
$ruta_ejecucion = "./intermedio_validacion_linea_base.xml";
$debug = new DebugClass();
$debug->create_log($log);

$intermedio_etiquetado = new ArchivoIntermedioClass();
$intermedio_etiquetado -> set_archivo_salida($ruta_etiquetado);
$intermedio_etiquetado -> set_salida($ruta_etiquetado);
$intermedio_etiquetado -> set_debug($debug);
$arr_etiquetado =  $intermedio_etiquetado -> leer();


$intermedio_ejecucion = new ArchivoIntermedioClass();
$intermedio_ejecucion -> set_archivo_salida($ruta_ejecucion);
$intermedio_ejecucion -> set_salida($ruta_ejecucion);
$intermedio_ejecucion -> set_debug($debug);
$arr_ejecucion =  $intermedio_ejecucion -> leer();



if (count($arr_ejecucion)!=count($arr_etiquetado) ||  count($arr_etiquetado) < 1) {
	print_r("Error");
	exit(1);
}		

// MEDIR ETAPA 2



for ($i = 0; $i < count($arr_ejecucion); $i++) {
	
$cant_oov = 0;
$cant_iv = 0;
$cant_palabras_normalizadas = 0; 
$cant_iv_ok = 0;
$cant_iv_marcadas = 0;
$cant_iv_real = 0;

	for ($i2 = 0; $i2 < count($arr_ejecucion[$i]); $i2++) {
		
		if ($arr_ejecucion[$i][$i2][1]==0&&$arr_etiquetado[$i][$i2][1]==0) {
			$cant_iv_ok++; 
		}
		
		if ($arr_ejecucion[$i][$i2][1]==0) {
			$cant_iv_marcadas++; 
			
		}

		if ($arr_etiquetado[$i][$i2][1]==0) {
			$cant_iv_real++; 
			$cant_oov++;
		
		}
		
		if (strtolower($arr_ejecucion[$i][$i2][0])!=strtolower($arr_etiquetado[$i][$i2][0])){
			print_r($arr_ejecucion[$i][$i2-2][0]."\n");
			print_r($arr_etiquetado[$i][$i2-2][0]."\n");
			print_r($arr_ejecucion[$i][$i2-1][0]."\n");
			print_r($arr_etiquetado[$i][$i2-1][0]."\n");
			print_r($arr_ejecucion[$i][$i2][0]."\n");
			print_r($arr_etiquetado[$i][$i2][0]."\n");
			print_r($arr_ejecucion[$i][$i2+1][0]."\n");
			print_r($arr_etiquetado[$i][$i2+1][0]."\n\n\n");

		}
		
		$cant_iv++;
		
	}
//}

$cant_iv = $cant_iv - $cant_oov;

$precision = round(100*$cant_iv_ok/$cant_iv_marcadas,2);
$cobertura = round(100*$cant_iv_ok/$cant_iv_real,2);
$f1 = round(((1+1)*$precision*$cobertura)/(1*$precision+$cobertura),2);

print_r("\n\n");
print_r("ETAPA 2\n");
print_r("	Precisión: ".$precision."\n");
print_r("	Cobertura: ".$cobertura."\n");
print_r("	F1: ".$f1."\n");



// MEDIR ETAPA 3

$cant_candidato_ok = 0;
$cant_candidatos_marcado = 0;
$cant_candidatos = 0;

for ($i = 0; $i < count($arr_ejecucion); $i++) {
	for ($i2 = 0; $i2 < count($arr_ejecucion[$i]); $i2++) {
		if ($arr_ejecucion[$i][$i2][1]==0&&$arr_etiquetado[$i][$i2][1]==0) {
			if (array_key_exists(2,$arr_ejecucion[$i][$i2]) && array_key_exists(2,$arr_etiquetado[$i][$i2]) && count($arr_ejecucion[$i][$i2])>0 && count($arr_etiquetado[$i][$i2])>0)
			{
				$band = true;
				//print_r($arr_etiquetado[$i][$i2][2]);
				if (count($arr_etiquetado[$i][$i2][2])>0){
					foreach ($arr_ejecucion[$i][$i2][2] as $candidato) {	
						if (strtolower($candidato[0])==strtolower($arr_etiquetado[$i][$i2][2][0][0]) && $band){
							$band = false;
							$cant_candidato_ok++;
						}
					}
					//if ($band==true){print_r("".$arr_etiquetado[$i][$i2][0]);}
					$cant_palabras_normalizadas++;
				}
				//$cant_candidatos_marcado++;
				//echo "1";
			}
			if (array_key_exists(2,$arr_ejecucion[$i][$i2]) && count($arr_ejecucion[$i][$i2][2])>0){
				$cant_candidatos_marcado++;
				//echo "1";
			}
			if (array_key_exists(2,$arr_etiquetado[$i][$i2]) && count($arr_etiquetado[$i][$i2][2])>0 ){
				$cant_candidatos++;
				///echo "2";
			}				
			
		}

	}
}

$precision = round(100*$cant_candidato_ok/$cant_candidatos_marcado,2);
$cobertura = round(100*$cant_candidato_ok/$cant_candidatos,2);
$f1 = round(((1+1)*$precision*$cobertura)/(1*$precision+$cobertura),2);

print_r("\n");
print_r("ETAPA 3\n");
print_r("	Precisión: ".$precision."\n");
print_r("	Cobertura: ".$cobertura."\n");
print_r("	F1: ".$f1."\n");


// MEDIR ETAPA 4

$cant_seleccion_realizadas_ok = 0;
$cant_seleccion_realizadas = 0;
$cant_seleccion = 0;

for ($i = 0; $i < count($arr_ejecucion); $i++) {
	for ($i2 = 0; $i2 < count($arr_ejecucion[$i]); $i2++) {
		if ($arr_ejecucion[$i][$i2][1]==0&&$arr_etiquetado[$i][$i2][1]==0) {
			if (array_key_exists(2,$arr_ejecucion[$i][$i2]) && array_key_exists(2,$arr_etiquetado[$i][$i2]))
			{
				$band = true;
				if (count($arr_etiquetado[$i][$i2][2])>0){
					foreach ($arr_ejecucion[$i][$i2][2] as $candidato) {
	//					print_r($candidato);
						if (strtolower($candidato[0])==strtolower($arr_etiquetado[$i][$i2][2][0][0]) && $band){
							$band = false;
							if($candidato[0]==$arr_ejecucion[$i][$i2][2][0][0]){
								$cant_seleccion_realizadas_ok++;
							}
						}
					}
					
				}
				if (count($arr_ejecucion[$i][$i2][2])>0){
					$cant_seleccion_realizadas++;
				}
				
				if (count($arr_etiquetado[$i][$i2][2])>0){
					$cant_seleccion++;
				}
			}	
			
		}
	}
}			

// MEDIR TOTAL

$cant_corregido_ok = 0;
$cant_corregido = 0;
$cant_a_corregir = 0;

$precision = round(100*$cant_seleccion_realizadas_ok/$cant_seleccion_realizadas,2);
$cobertura = round(100*$cant_seleccion_realizadas_ok/$cant_seleccion,2);
$f1 = round(((1+1)*$precision*$cobertura)/(1*$precision+$cobertura),2);

print_r("\n");
print_r("ETAPA 4\n");
print_r("	Precisión: ".$precision."\n");
print_r("	Cobertura: ".$cobertura."\n");
print_r("	F1: ".$f1."\n");


for ($i = 0; $i < count($arr_ejecucion); $i++) {
	for ($i2 = 0; $i2 < count($arr_ejecucion[$i]); $i2++) {
		if ($arr_ejecucion[$i][$i2][1]==0&&$arr_etiquetado[$i][$i2][1]==0) {
			if (array_key_exists(2,$arr_ejecucion[$i][$i2]) && array_key_exists(2,$arr_etiquetado[$i][$i2])){
				$band = true;
				if (count($arr_etiquetado[$i][$i2][2])>0){
					if(strtolower($arr_ejecucion[$i][$i2][2][0][0])==strtolower($arr_etiquetado[$i][$i2][2][0][0])) {
						$cant_corregido_ok++;
					}
				}
			}
		}
		
		if ($arr_ejecucion[$i][$i2][1]==0) {
			if (array_key_exists(2,$arr_ejecucion[$i][$i2]) && count($arr_ejecucion[$i][$i2][2])>0){
					$cant_corregido++;
				}
			}

		if ($arr_etiquetado[$i][$i2][1]==0) {
			if (array_key_exists(2,$arr_etiquetado[$i][$i2]) && count($arr_etiquetado[$i][$i2][2])>0){
					$cant_a_corregir++;
				}
			}		
	}


print_r("\n");
print_r("Resumen MSJ".($i+1)."\n");
print_r("	Cantidad Palabras: ".($cant_iv+$cant_oov)."\n");
print_r("	Cantidad Palabras IV: ".$cant_iv."\n");
print_r("	Cantidad Palabras OV: ".$cant_oov."\n");
print_r("	Cantidad Palabras corregidas: ".$cant_palabras_normalizadas."\n");
	
}

$precision = round(100*$cant_corregido_ok/$cant_corregido,2);
$cobertura = round(100*$cant_corregido_ok/$cant_a_corregir,2);
$f1 = round(((1+1)*$precision*$cobertura)/(1*$precision+$cobertura),2);

print_r("\n");
print_r("TOTAL\n");
print_r("	Precisión: ".$precision."\n");
print_r("	Cobertura: ".$cobertura."\n");
print_r("	F1: ".$f1."\n");

print_r("\n\n");


print_r("\n");
print_r("Resumen Data Set\n");
print_r("	Cantidad Palabras: ".($cant_iv+$cant_oov)."\n");
print_r("	Cantidad Palabras IV: ".$cant_iv."\n");
print_r("	Cantidad Palabras OV: ".$cant_oov."\n");
print_r("	Cantidad Palabras corregidas: ".$cant_palabras_normalizadas."\n");

print_r("	Cantidad Palabras Marcadas IV: ".$cant_iv_marcadas."\n");
print_r("	Cantidad Palabras Marcadas OV: ".($cant_iv+$cant_oov-$cant_iv_marcadas)."\n");
print_r("	Cantidad Palabras Marcadas Corregidas: ".$cant_corregido."\n");

print_r("\n\n");*/

?>
