<?php 

/**
* @file main.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Programa Principal: Normalizador para tweets en Español.
* Bloques Principales:
* 	- Encabezado de declaraciones.
* 	- Lectura de Archivo de Configuración.
* 	- Lectura de las rutas a los archivos y a las etapas.
* 	- Inicialización de los módulos.
* 	- Inicialización de archivo intermedio.
* 	- Ejecución de cada una de las etapas: 1..5.
* 
*/

/* ========== ENCABEZADO DE DECLARACIONES ========== */
   

include("./autoloader.php"); // Autoloader
include("./Lib/LibExt/time_elapsed.php"); // Time Elapsed Function

use \Lib\LibInt\DebugClass;
use \Lib\LibInt\ArchivoIntermedioClass;
use \Lib\LibInt\ArchivoConfiguracionClass;
use \Lib\LibInt\ArchivoMemoriaClass;
use \Lib\LibInt\AuxiliarClass;
use \Lib\LibInt\EtapaClass;
use \Lib\LibInt\Etapa2Class;
use \Lib\LibInt\Etapa3Class;
use \Lib\LibInt\Etapa4Class;

/* ========== ENCABEZADO DE DECLARACIONES ========== */

/* ========== ARCHIVO DE CONFIGURACIÓN ========== */
$config = new ArchivoConfiguracionClass();
$config -> inicializar("./main.config");
$arr_config = $config -> leer();
/* ========== ARCHIVO DE CONFIGURACIÓN ========== */

/* ========== RUTAS A LOS ARCHIVOS ========== */
$log = (string)$arr_config -> archivo_log;
$archivo_entrada = (string)$arr_config -> archivo_entrada;
$archivo_salida = (string)$arr_config -> archivo_salida;
$archivo_intermedio = (string)$arr_config -> archivo_intermedio;
/* ========== RUTAS A LOS ARCHIVOS ========== */

/* ========== RUTAS A LAS ETAPAS ========== */
$ruta_etapa1 = (string)$arr_config -> etapa1 -> ruta;
$ruta_etapa2 = (string)$arr_config -> etapa2 -> ruta;
$ruta_etapa3 = (string)$arr_config -> etapa3 -> ruta;
$ruta_etapa4 = (string)$arr_config -> etapa4 -> ruta;
$ruta_etapa5 = (string)$arr_config -> etapa5 -> ruta;
$ruta_auxiliar = (string)$arr_config -> auxiliar -> ruta;
/* ========== RUTAS A LAS ETAPAS ========== */

/* ========== SETEO DE MODULOS POR ETAPA ========== */
$modulos_etapa1 = (array)$arr_config -> etapa1 -> modulo;

$modulos_auxiliar = (array)$arr_config -> auxiliar -> modulo;

$modulos_etapa2 = (array)$arr_config -> etapa2 -> modulo;

$modulos_etapa3 = (array)$arr_config -> etapa3 -> modulo;

$modulos_etapa4 = (array)$arr_config -> etapa4 -> modulo;

$modulos_etapa5 = (array)$arr_config -> etapa5 -> modulo;



/* ========== SETEO DE MODULOS POR ETAPA ========== */

/* ========== INICIALIZACION ========== */
$tiempo = time();
$debug = new DebugClass();
$debug->create_log($log);

$intermedio = new ArchivoIntermedioClass();
$intermedio -> inicializar($archivo_entrada, $archivo_intermedio,  $debug);
$intermedio -> set_archivo_salida($archivo_salida);
/* ========== INICIALIZACION ========== */


/* ========== EJECUCION DE ETAPAS ========== */
print_r("\n Comenzando main");


print_r("\n Configurando Auxiliar");
$tiempo2 = time();
$auxiliar = new AuxiliarClass();
$auxiliar -> inicializar (null,$modulos_auxiliar,'\Etapas\Auxiliar\\',true,$debug);
print_r("\n Finalizando Auxiliar (".(time_elapsed(time()-$tiempo2)).")");


print_r("\n Comenzando Etapa 1");
$tiempo2 = time();
$etapa1 = new EtapaClass();
$etapa1 -> inicializar ($intermedio,$modulos_etapa1,'\Etapas\Etapa1\\',true,$debug);
$etapa1 ->ejecutar();
print_r("\n Finalizando Etapa 1 (".(time_elapsed(time()-$tiempo2)).")");

print_r("\n Comenzando Etapa 2");
$tiempo2 = time();
$etapa2 = new Etapa2Class();
$etapa2 -> inicializar ($intermedio,$modulos_etapa2,'\Etapas\Etapa2\\',$auxiliar,true,$debug);
$etapa2 ->ejecutar();
print_r("\n Finalizando Etapa 2 (".(time_elapsed(time()-$tiempo2)).")");

print_r("\n Comenzando Etapa 3");
$tiempo2 = time();
$etapa3 = new Etapa3Class();
$etapa3 -> inicializar ($intermedio,$modulos_etapa3,'\Etapas\Etapa3\\',$auxiliar,true,$debug);
$etapa3 ->ejecutar();
print_r("\n Finalizando Etapa 3 (".(time_elapsed(time()-$tiempo2)).")");

print_r("\n Comenzando Etapa 4");
$tiempo2 = time();
$etapa4 = new Etapa4Class();
$etapa4 -> inicializar ($intermedio,$modulos_etapa4,'\Etapas\Etapa4\\',true,$debug);
$etapa4 ->ejecutar();
print_r("\n Finalizando Etapa 4 (".(time_elapsed(time()-$tiempo2)).")");

print_r("\n Comenzando Etapa 5");
$tiempo2 = time();
$etapa5 = new EtapaClass();
print_r("\n Finalizando Etapa 5 (".(time_elapsed(time()-$tiempo2)).")");

/* ========== EJECUCION DE ETAPAS ========== */

print_r("\n Duracion: ".(time_elapsed(time()-$tiempo)));
print_r("\n Finalizado main \n");

$intermedio -> set_debug($debug);
$intermedio -> generar_salida();

?>

