<?php

/**
* @file time_elapsed.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Función usada para calcular y formatear el tiempo transcurrido.
* 
*/

/**
* @brief Time Elapsed: Funcion para calcular el tiempo de ejecución.
* @param $secs. Integer. Segundos.
* @return string.
*/
function time_elapsed($secs){
    $bit = array(
        'y' => $secs / 31556926 % 12,
        'w' => $secs / 604800 % 52,
        'd' => $secs / 86400 % 7,
        'h' => $secs / 3600 % 24,
        'm' => $secs / 60 % 60,
        's' => $secs % 60
        );
        
    $ret = array();
    foreach($bit as $k => $v)
        if($v > 0) $ret[] = $v . $k;
       
    if (count($ret)>0){
		//print_r($ret);
		return join(' ', $ret);
	}
	else{
		return '0s';
	}
};
