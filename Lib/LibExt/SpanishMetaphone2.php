<?php
/**
* @file SpanishMetaphone.php
* @Author Israel J. Sustaita (isloera@yahoo.com)
* @date 2013-01-08
* @brief Spanish Metaphone.
*/

/**
* Spanish Metaphone.
* @see spanish_metaphone().
* @param $string. A string.
* @return Metaphone key string.
* @Author Israel J. Sustaita (isloera@yahoo.com)
* @date 2013-01-08
* @brief This function takes a spanish word and returns its
* metaphone sound key.
* Comments: It generates spanish  metaphone  keys useful for  spell 
* checkers and other purposes.I decided to alter the 
* metaphone function   because  I needed to check the spelling 
* in spanish words.
* History:
*          2013-01-08 - versión 1.0.2
*             - Agregados caracteres internacionales para manejar los multibytes
*             - Agregados bugs menores
*          2005-10-14 - Version 1.0.1 
*             - Removed unnecesary code and fixed some minor bugs
*
*          2005-10-09 - Version 1.0.0 
*             - Initial Release 
*
*
*                **** Acknowledgements ****
*
*  This Function was adapted from a functional callable version of
*  DoubleMetaphone created by Geoff Caplan http://www.advantae.com, who 
*  adapted it from the class by  Stephen Woodbridge.
*
*
*  It also uses the "string_at()" and the "is_vowel()" functions  from 
*  the same implementation.
*            
*           Source:  http://swoodbridge.com/DoubleMetaPhone/
*           Actualizado de http://www.calvilloweb.com/spanish_metaphone.php
*/
function spanish_metaphone($string)
{
   $normalizeChars = array( 
      'Á'=>'A', 'À'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Å'=>'A', 'Ä'=>'A', 'Æ'=>'AE', 'Ç'=>'C', 
      'É'=>'E', 'È'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Í'=>'I', 'Ì'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ð'=>'Eth', 
      'Ñ'=>'N', 'Ó'=>'O', 'Ò'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 
      'Ú'=>'U', 'Ù'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 
   
      'á'=>'a', 'à'=>'a', 'â'=>'a', 'ã'=>'a', 'å'=>'a', 'ä'=>'a', 'æ'=>'ae', 'ç'=>'c', 
      'é'=>'e', 'è'=>'e', 'ê'=>'e', 'ë'=>'e', 'í'=>'i', 'ì'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'eth', 
      'ñ'=>'n', 'ó'=>'o', 'ò'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 
      'ú'=>'u', 'ù'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 
      
      'ß'=>'sz', 'þ'=>'thorn', 'ÿ'=>'y' 
   );
   //initialize metaphone key string
   $meta_key   = "";
   
   //set maximum metaphone key size   
   $key_length   = 12;
   
   //set current position to the beginning
   $current_pos   =  0;
   
   //get string  length
   $string_length   = strlen($string);
   
   //set to  the end of the string
   $end_of_string_pos     = $string_length - 1;    
   $original_string = $string. "    ";
   
   //Let's replace some spanish characters  easily confused
   $original_string = strtr($original_string,$normalizeChars);
   
   //convert string to uppercase
   $original_string = strtoupper($original_string);       
   
   
   // main loop
   while (strlen($meta_key) < $key_length) 
   {
         
      //break out of the loop if greater or equal than the length
      if ($current_pos >= $string_length)
      {
         break;
      }
        
      //get character from the string
      $current_char = substr($original_string, $current_pos, 1);
      
      //if it is a vowel, and it is at the begining of the string,
      //set it as part of the meta key        
      if (is_vowel($original_string, $current_pos)
                            && ($current_pos == 0))
      {
         $meta_key   .= $current_char;            
         $current_pos += 1;            
      }        
      else if ($current_pos != 0 && substr($original_string, $current_pos - 1,1) == $current_char){
		   $current_pos += 1;    
	  }   
      //Let's check for consonants  that have a single sound 
      //or already have been replaced  because they share the same
      //sound like 'B' for 'V' and 'S' for 'Z'
      else if (string_at($original_string, $current_pos, 1, 
              array('D','F','J','K','N','S','B'))) 
        {
         $meta_key   .= $current_char; 
         
         //increment by two if a repeated letter is found
         //if (substr($original_string, $current_pos + 1,1) == $current_char) 
         //{                     
         //   $current_pos += 2;             
         //}  
            
         //else increment only by one                 
         $current_pos += 1;            
      }
      else  //check consonants with similar confusing sounds
      {
         
         switch ($current_char) 
         {		
            case 'C':  
               //special case 'macho', chato,etc.      
               if (substr($original_string, $current_pos + 1,1)== 'H')
               {               
				 $meta_key   .= 'CH';                       
                  $current_pos += 2;
                  break;                                  
               }      
               //special case 'acción', 'reacción',etc.      
               else if (substr($original_string, $current_pos + 1,1)== 'C')
               {                                        
                     
                  $meta_key   .= 'KS';            
                  $current_pos += 2;
                  break;                                
               }          
               // special case 'cesar', 'cien', 'cid', 'conciencia'
               else if (string_at($original_string, $current_pos, 2, 
                         array('CE','CI'))) 
               {
                  $meta_key   .= 'S';            
                  $current_pos += 2;
                  break;
               }
               // else
               $meta_key   .= 'K';                   
               $current_pos += 1;            
               break;     
               
            case 'G':
               if ($current_pos == 0 and substr($original_string, $current_pos + 1,1)== 'N') 
               {                    
                  $meta_key   .= 'N';
                  $current_pos += 2; 
                  break;          
               } 
               // special case 'gente', 'ecologia',etc 
               if (string_at($original_string, $current_pos, 2, 
                         array('GE','GI')))
               {
                  $meta_key   .= 'J';            
                  $current_pos += 2;
                  break;
               }
               // else
               $meta_key   .= 'G';                   
               $current_pos += 1;            
               break;
          
            //since the letter 'h' is silent in spanish, 
            //let's set the meta key to the vowel after the letter 'h'
            case 'H':                
               if (is_vowel($original_string, $current_pos + 1))
               {
                  $meta_key .= $original_string[$current_pos + 1];            
                  $current_pos += 2;
                  break;
               } 
                      
               // else
               //$meta_key   .= 'H';                   
               $current_pos += 1;            
               break;    
               
            case 'Q':
               if (substr($original_string, $current_pos + 1,1) == 'U' OR substr($original_string, $current_pos + 1,1) == 'A')
               { 
                  $current_pos += 2;
               }
               else 
               {
                  $current_pos += 1;
               }
				$meta_key   .= 'K';  
                       
               break;
                  
             case 'L':
               if (substr($original_string, $current_pos + 1,1) == 'L')
               { 
                  $current_pos += 2;
                  $meta_key   .= 'Y';  
               }
               else 
               {
                  $current_pos += 1;
                  $meta_key   .= 'L';  
               }
                    
               break;  
               
             case 'M':
               if ($current_pos == 0 and substr($original_string, $current_pos + 1,1)== 'N') 
               {                    
                  $meta_key   .= 'N';
                  $current_pos += 2; 
                  break;          
               }              
               if (substr($original_string, $current_pos + 1,1) == 'B' OR substr($original_string, $current_pos + 1,1) == 'P' )
               { 
                  $current_pos += 1;
                  $meta_key   .= 'N';  
               }
               else 
               {
                  $current_pos += 1;
                  $meta_key   .= 'M';  
               }
                    
               break; 

             case 'R':
               if (substr($original_string, $current_pos + 1,1) == 'R')
               { 
                  $current_pos += 2;

               }
               else 
               {
                  $current_pos += 1;
               }
               $meta_key   .= 'R';  
               break;  

              case 'F':
               if ($current_pos == 0 and substr($original_string, $current_pos + 1,1)== 'T') 
               {                    
                  $meta_key   .= 'T';
                  $current_pos += 2; 
                  break;          
               } 
			   $meta_key   .= 'F'; 
               $current_pos += 1;
                
               break; 
 
               case 'T':
               if ($current_pos == 0 and substr($original_string, $current_pos + 1,1)== 'S') 
               {                    
                  $meta_key   .= 'S';
                  $current_pos += 2; 
                  break;          
               } 
			   $meta_key   .= 'T'; 
               $current_pos += 1;
                
               break; 
                              
              case 'P':
               if ($current_pos == 0 and substr($original_string, $current_pos + 1,1)== 'S') 
               {                    
                  $meta_key   .= 'S';
                  $current_pos += 2; 
                  break;          
               } 
               
               if ($current_pos == 0 and substr($original_string, $current_pos + 1,1)== 'T') 
               {                    
                  $meta_key   .= 'T';
                  $current_pos += 2; 
                  break;          
               } 
               if (substr($original_string, $current_pos + 1,1) == 'T')
               { 
				  $meta_key   .= 'G'; 
                  $current_pos += 1;

               }
               else 
               {
				   $meta_key   .= 'P'; 
                  $current_pos += 1;
               }
                
               break;  
                             
            case 'W':          
               $meta_key   .= 'GU';            
               $current_pos += 1;
               break;          
            
            case 'V':          
               $meta_key   .= 'B';            
               $current_pos += 1;
               break;   
                  
            case 'X': 
               //some mexican spanish words like'Xochimilco','xochitl'         
               if ($current_pos == 0) 
               {                    
                  $meta_key   .= 'S';
                  $current_pos += 2; 
                  break;          
               } 
                          
               $meta_key   .= 'KS';
               $current_pos += 1; 
               break;         
 
             case 'Z': 
               //some mexican spanish words like'Xochimilco','xochitl'         
                          
               $meta_key   .= 'S';
               $current_pos += 1; 
               break; 

             case 'Y':
               if (is_vowel($original_string, $current_pos + 1))
               { 
                  $current_pos += 1;
                  $meta_key   .= 'Y';  

               }
               else 
               {
                  $current_pos += 1;
                  $meta_key   .= 'I'; 
               }
      
               break;  
                             
            default:
               $current_pos += 1;
               
         } // end of switch
            
         
      }//end else       
        
      
      //Commented code *** for debugging purposes only ***
      /*/
      printf("ORIGINAL STRING:    '%s'\n", $original_string);
      printf("CURRENT POSITION:   '%s'\n", $current_pos);
      printf("META KEY STRING:    '%s'\n", $meta_key);
      /**/
      
   } // end of while loop
       
    
   //trim any blank characters
   $meta_key = trim($meta_key) ;
   
   //return the final meta key string
   return $meta_key;
   
}
// ====== End of spanish_metaphone function =======================

  
//***** helper functions *******************************************
//****************************************************************** 

/**
* String At?.
* @param $string. A string.
* @param $string_lenght. A integer.
* @param $list. A array.
* @param $pos. Integer.
* @return Boolean.
* @Author Israel J. Sustaita (isloera@yahoo.com).
* @brief Helper function for double_metaphone( ).
*/
function string_at($string, $start, $string_length, $list)  {
   if (($start <0) || ($start >= strlen($string))) {
      return 0;
   }
   $subs = substr($string, $start, $string_length);
   for ($i=0; $i<count($list); $i++) {
      if ($list[$i] == $subs) {
         return 1;
      }
   }
   return 0;
}

/**
* Is Vowel?.
* @param $string. A string.
* @param $pos. Integer.
* @return Boolean.
* @Author Israel J. Sustaita (isloera@yahoo.com)
*/
function is_vowel($string, $pos) {
    return ereg("[AEIOU]", substr($string, $pos, 1));
}
// ******** end of helper functions **************************

/**
$palabras = ['lindo', 'lendo', 'qqqqqqqq','que'];
foreach($palabras as $word)	{
	echo $word . ' -> ' . spanish_metaphone($word) . "\n";
}
/**/

/*

Fonemas a tener en cuenta: B, CH, D, F, G, J, K, L, M, N, P, R, S, T, W, Y

C se transforma en S si CE o CI. De lo contrario se transforma en K
G se transforma en J si GE o GI.
LL se transforma en Y.
M se transforma en N si MB o MP.
Q se transforma en K si QA, QUE o QUI.
RR se transforma en R

V se transforma en B.
W se transforma en GU.
X se transforma en S si al inicio de palabra. De lo contrario X se transforma en KS
Z se transforma en S.

Y se transforma en I si no es seguida de vocal.
P se transforma en G si PT.
Si la palabra comienza con GN, PS, FT, MN, PT, TS se tiene en cuenta sólo la segunda letra.
Ejemplos: Gnoseología, psicoanálisis, ftálico, mnemotecnia, ptialismo, tsunami
Así:
GN se transforma en N
PS se transforma en S
FT se transforma en T
MN se transforma en N
PT se transforma en T
TS se transforma en S
Los restantes se eliminan.
*/
