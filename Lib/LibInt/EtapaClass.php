<?php

/** @file EtapaClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase EtapaClass. Clase que tiene la abstracción de una etapa.
*/

namespace Lib\LibInt;

/**
 *  EtapaClass. Clase que tiene la abstracción de una etapa.
 */
class EtapaClass extends BaseAbstractClass
{
 	protected $salida = array(); // Objeto que apunta que representa la entrada y salida.
	protected $modulos= array(); // Listado ordenado de modulos a ejecutar
	protected $namespace = ''; // Espacio de nombres en el que se encuentran los modulos a ejecutar
	protected $acent_sensitive= false; // Indica si la comparación será sensitiva a los acentos
 	
 	/**
	* @brief Inicializa la clase.
	* @param $p_salida. Es un objeto de la clase EntradaSalidaClass. A traves de el se registrará la entrada y salida del modulo.
	* @param $p_modulos Listado de módulos a ejecutar.
	* @param $p_namespace. Espacio de nombre de los módulos.
	* @param $p_boolean. Es un objeto de la clase ArchivoIntermedioClass. A traves de el se registrará la entrada y salida del modulo.
	* @param $p_debug. Es un objeto de la clase DebugClass. A traves de el se registraran los eventos del sistema
	* @return nothing.
	*/	
 	public function inicializar($p_salida, $p_modulos, $p_namespace, $p_boolean, $p_debug) {	
		$this -> set_debug($p_debug);
		$this -> set_namespace($p_namespace);
		$this -> set_salida($p_salida);
		$this -> set_acent_sensitive($p_boolean);
		$this -> set_modulos($p_modulos);
	}

   
 	/**
	* @brief Método abstracto. El programá principal llamará a este método. 
	* @return nothing.
	*/
	public function ejecutar(){
		$this -> entrar_debug("ejecutar");
		foreach ($this->modulos as $modulo){
			//print_r("\n  Comenzando modulo: ".$this->namespace.$modulo);
			//$tiempo = time();
			//eval('$i = new '.$this->namespace.$modulo.';');
			//$i-> inicializar ($this->salida,$this->acent_sensitive, $this->debug);
			//$i-> set_lazzy($this->lazzy);
			$modulo ->ejecutar();
			//print_r("\n  Finalizando modulo: ".$this->namespace.$modulo." (".($this->time_elapsed(time()-$tiempo)).")");
		}
		$this -> salir_debug("ejecutar");
	}


	/**
	* @brief Setea la variable salida con un objeto del tipo EntradaSalidaClass.
	* @param $p_salida. Es un objeto de la clase EntradaSalidaClass. A traves de el se registrará la entrada y salida del modulo.
	* @return nothing.
	*/
	public function set_salida($p_salida){
		$this -> entrar_debug("set_salida");
		$this -> salida = $p_salida;
		foreach ($this->modulos as &$modulo){
			$modulo->set_salida($p_salida);
		}
		$this -> salir_debug("set_salida");
	}


	/**
	* @brief Setea la variable modulos con el listado de los modulos a ejecutar.
	* @param $p_modulos. Arreglo con el formato: array(NombreModulo1, NombreModulo2, ...)
	* @return nothing.
	*/
	public function set_modulos($p_modulos){
		$this -> entrar_debug("set_modulos");
		$this -> modulos = array();
		foreach ($p_modulos as $modulo){
			//print_r("\n  Comenzando modulo: ".$this->namespace.$modulo);
			$tiempo = time();
			eval('$i = new '.$this->namespace.$modulo.';');
			$i-> inicializar ($this->salida,$this->acent_sensitive, $this->debug);

			$this ->modulos[]= $i;
		}
		$this -> salir_debug("set_modulos");
	}

	/**
	* @brief Setea la variable namespace con el espacio de nombres de los modulos.
	* @param $p_namespace. String con el espacio de nombre y/o la ruta a la clase.
	* @return nothing.
	*/
	public function set_namespace($p_namespace){
		$this -> entrar_debug("set_namespace");
		$this -> namespace = $p_namespace;
		$this -> salir_debug("set_namespace");
	}

	public function set_acent_sensitive($p_boolean){
	/**
	* @brief Setea la variable acent_sensitive que indica si se tendrán en cuenta los acentos o no.
	* @param $p_boolean. Booleano.
	* @return nothing.
	*/
		$this -> entrar_debug("set_archivo_intermedio");
		foreach ($this->modulos as &$modulo){
			$modulo->set_acent_sensitive($p_boolean);
		}
		$this -> acent_sensitive = $p_boolean;
		$this -> salir_debug("set_archivo_intermedio");
	}
	
}

