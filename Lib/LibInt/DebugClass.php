<?php
/** @file DebugClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase DebugClass. Clase responsable del registro de los eventos del sistema.
*/

namespace Lib\LibInt;

/**
 *  DebugClass. Clase responsable del registro de los eventos del sistema.
 */

class DebugClass {

	protected $debug_mode= false; // Indica si el modo debug es exahustivo. 
	protected $log; // Objeto del tipo LogClass responsable de la visualización del evento. 

	/**
	* @brief Setea la variable debug_mode con un objeto del tipo boolean.
	* @param $p_debug_mode. Es un booleano que indica si el modo debug exahustivo está habilitado. 
	* @return nothing.
	*/
	public function set_debug_mode($p_debug_mode) {
		$this -> debug_mode = $p_debug_mode;
	}

	/**
	* @brief Setea la variable log con un objeto del tipo LogClass.
	* @param $p_log. Es un objeto del tipo LogClass. 
	* @return nothing.
	*/
	public function set_log($p_log) {
		$this -> log = $p_log;
	}

	/**
	* @brief Se envia la ruta y es responble de la creación de un nuevo objeto LogClass.
	* @param $p_ruta. Es un objeto del tipo string que es la ruta al archivo log. 
	* @return nothing.
	*/
	public function create_log($p_ruta) {
		$this -> log = new LogClass();
		$this -> log -> set_ruta($p_ruta);
		$this -> log -> borrar();
	}

	/**
	* @brief Registra un evento de entrada en el log.
	* @see entrar().
	* @param $nombre. Mensaje a registrar en el log.
	* @return nothing.
	*/
	public function entrar($nombre) {
		if ($this->debug_mode == true) {
			$this -> log -> escribir("Debug: Entrando a: ".$nombre.".\n");
		}
	}

	/**
	* @brief Registra un evento de salida en el log.
	* @param $nombre. Mensaje a registrar en el log.
	* @return nothing.
	*/
	public function salir($nombre) {
		if ($this->debug_mode == true) {
			$this->log->escribir("Debug: Saliendo de: ".$nombre.".\n");
			return true;
		}
	}

	/**
	* @brief Registra un evento de warning en el log.
	* @param $nombre. Mensaje a registrar en el log.
	* @return nothing.
	*/
	public function warn($mensaje) {
		$this->log->escribir("Warn: ".$mensaje.".\n");
		return true;
	}

	/**
	* @brief Registra un evento de error en el log.
	* @param $nombre. Mensaje a registrar en el log.
	* @return nothing.
	*/
	public function error($mensaje) {
		$this->log->escribir("Error: ".$mensaje.".\n");
		return true;
	}

}
