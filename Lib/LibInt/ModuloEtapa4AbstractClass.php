<?php

/** @file ModuloEtapa3AbstractClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase ModuloAEtapa4bstractClass. Clase base para los módulos de la etapa 3.
*/

namespace Lib\LibInt;

/**
 *  ModuloAEtapa4bstractClass. Clase base para los módulos de la etapa 4.
 */
abstract class ModuloEtapa4AbstractClass extends ModuloAbstractClass
{

	/**
	* @brief Inicializa la clase.
	* @param $p_salida Es un objeto de la clase EntradaSalidaClass. A traves de el se registrará la entrada y salida del modulo.
	* @param $p_acent_sensitive. Booleando que indica si el módulo será o no sensitivo a los acentos.
	* @param $p_etapa2 Es un objeto de la clase EtapaClass. Se utilizará para detectar si la palabra candidata encontrada es IV
	* @param $p_bug. Es un objeto de la clase DebugClass. A traves de el se registraran los eventos del sistema
	* @return nothing.
	*/
 	public function inicializar($p_salida, $p_acent_sensitive, $p_debug) {

		$this -> entrar_debug("inicializar");

		$this -> set_debug($p_debug);
		$this -> set_salida($p_salida);
		$this -> set_acent_sensitive($p_acent_sensitive);

		$this -> salir_debug("inicializar");

	}

}

