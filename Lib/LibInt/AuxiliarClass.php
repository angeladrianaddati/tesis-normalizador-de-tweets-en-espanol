<?php

/** @file AuxiliarClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase AuxiliarClass. Clase que tiene la abstracción de la etapa auxiliar.
*/

namespace Lib\LibInt;

include __DIR__ . '/../LibExt/SpanishMetaphone2.php';

/**
 *  AuxiliarClass. Clase que tiene la abstracción de la etapa auxiliar.
 */
class AuxiliarClass extends EtapaClass
{

 	protected $lazzy= false; // Indica si la comparación será relajada
 	protected $fonetic= false; // Indica si la comparación será fonetica
 	protected $levenshtein= false; // Indica si la comparación será por distancia de edicion
 	
 	/**
	* @brief Inicializa la clase.
	* @param $p_salida. Es un objeto de la clase EntradaSalidaClass. A traves de el se registrará la entrada y salida del modulo.
	* @param $p_modulos Listado de módulos a ejecutar.
	* @param $p_namespace. Espacio de nombre de los módulos.
	* @param $p_boolean. Es un objeto de la clase ArchivoIntermedioClass. A traves de el se registrará la entrada y salida del modulo.
	* @param $p_debug. Es un objeto de la clase DebugClass. A traves de el se registraran los eventos del sistema
	* @return nothing.
	*/	
 	public function inicializar($p_salida, $p_modulos, $p_namespace, $p_boolean, $p_debug) {	
		$this -> set_debug($p_debug);
		$this -> set_namespace($p_namespace);
		$this -> set_salida($p_salida);
		$this -> set_acent_sensitive($p_boolean);
		$this -> set_lazzy(false);
		$this -> set_fonetic(false);
		$this -> set_levenshtein(false);
		$this -> set_modulos($p_modulos);
	}
   
 	/**
	* @brief Método abstracto. El programá principal llamará a este método. 
	* @return nothing.
	*/
	public function ejecutar(){
		$this -> entrar_debug("ejecutar");
		foreach ($this->modulos as $modulo){
			//print_r("\n  Comenzando modulo: ".$this->namespace.$modulo);
			//$tiempo = time();
			//eval('$i = new '.$this->namespace.$modulo.';');
			//$i-> inicializar ($this->salida,$this->acent_sensitive, $this->debug);
			//$i-> set_lazzy($this->lazzy);
			$modulo ->ejecutar();
			//print_r("\n  Finalizando modulo: ".$this->namespace.$modulo." (".($this->time_elapsed(time()-$tiempo)).")");
		}
		$this -> salir_debug("ejecutar");
	}


	/**
	* @brief Setea la variable modulos con el listado de los modulos a ejecutar.
	* @param $p_modulos. Arregloc on el formato: array(NombreModulo1, NombreModulo2, ...)
	* @return nothing.
	*/
	public function set_modulos($p_modulos){
		$this -> entrar_debug("set_modulos");
		$this -> modulos = array();
		foreach ($p_modulos as $modulo){
			//print_r("\n  Comenzando modulo: ".$this->namespace.$modulo);
			$tiempo = time();
			eval('$i = new '.$this->namespace.$modulo.';');
			$i-> inicializar ($this->salida,$this->acent_sensitive, $this->debug);
			$i-> set_lazzy($this->lazzy);

			$this ->modulos[]= $i;
		}
		$this -> salir_debug("set_modulos");
	}

	/**
	* @brief Setea la variable lazzy que indica si se tendrán en cuenta las letras y no el orden para la comparación.
	* @param $p_boolean. Booleano.
	* @return nothing.
	*/
	public function set_lazzy($p_boolean){
		$this -> entrar_debug("set_archivo_intermedio");
		$this -> lazzy = $p_boolean;
		foreach ($this->modulos as &$modulo){
			$modulo->set_lazzy($p_boolean);
		}
		$this -> salir_debug("set_archivo_intermedio");
	}

	/**
	* @brief Setea la variable levenshtein que indica si se hará una comparación entre palabras por parecido fonético.
	* @param $p_boolean. Booleano.
	* @return nothing.
	*/
	public function set_fonetic($p_boolean){
		$this -> entrar_debug("set_archivo_intermedio");
		$this -> fonetic = $p_boolean;
		foreach ($this->modulos as &$modulo){
			$modulo->set_fonetic($p_boolean);
		}
		$this -> salir_debug("set_archivo_intermedio");
	}

	/**
	* @brief Setea la variable levenshtein que indica si se hará una comparación entre palabras usando la distancia de edición.
	* @param $p_boolean. Booleano.
	* @return nothing.
	*/
	public function set_levenshtein($p_boolean){
		$this -> entrar_debug("set_archivo_intermedio");
		$this -> levenshtein= $p_boolean;
		foreach ($this->modulos as &$modulo){
			$modulo->set_levenshtein($p_boolean);
		}
		$this -> salir_debug("set_archivo_intermedio");
	}
	
	
}

