<?php

/** @file Etapa4Class.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase Etapa4Class. Clase que tiene la abstracción de la etapa 4.
 */
namespace Lib\LibInt;

/**
 *  Etapa4Class. Clase que tiene la abstracción de la etapa 4.
 */
class Etapa4Class extends EtapaClass
{

	/**
	* @brief Inicializa la clase.
	* @param $p_salida. Es un objeto de la clase EntradaSalidaClass. A traves de el se registrará la entrada y salida del modulo.
	* @param $p_modulos Listado de módulos a ejecutar.
	* @param $p_namespace. Espacio de nombre de los módulos.
	* @param $p_boolean. Es un objeto de la clase ArchivoIntermedioClass. A traves de el se registrará la entrada y salida del modulo.
	* @param $p_debug. Es un objeto de la clase DebugClass. A traves de el se registraran los eventos del sistema
	* @return nothing.
	*/
 	public function inicializar($p_salida, $p_modulos, $p_namespace, $p_boolean, $p_debug) {
		
		$this -> set_debug($p_debug);
		$this -> set_namespace($p_namespace);
		$this -> set_salida($p_salida);
		$this -> set_acent_sensitive($p_boolean);
		$this -> set_modulos($p_modulos);
	}

   
	/**
	* @brief Setea la variable modulos con el listado de los modulos a ejecutar.
	* @param $p_modulos. Arregloc on el formato: array(NombreModulo1, NombreModulo2, ...)
	* @return nothing.
	*/
	public function set_modulos($p_modulos){

		$this -> entrar_debug("set_modulos");
		$this -> modulos = array();
		foreach ($p_modulos as $modulo){
			//print_r("\n  Comenzando modulo: ".$this->namespace.$modulo);
			$tiempo = time();
			eval('$i = new '.$this->namespace.$modulo.';');
			$i->inicializar ($this->salida, $this->acent_sensitive,$this->debug);
			$this ->modulos[]= $i;
		}
		$this -> salir_debug("set_modulos");
	}


}

