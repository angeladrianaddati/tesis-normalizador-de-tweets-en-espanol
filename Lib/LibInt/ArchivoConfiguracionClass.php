<?php

/** @file ArchivoConfiguracionClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase ArchivoConfiguraciónClass. Clase responsable de la lectura y escritura de los archivos de configuración. El archivo de configuración será leído a iniciar la aplicación. 
*/
namespace Lib\LibInt;

/**
 *  ArchivoConfiguracionClass. Clase responsable de la lectura y escritura de los archivos de configuración. El archivo de configuración será leído a iniciar la aplicación. 
 */

class ArchivoConfiguracionClass extends BaseAbstractClass{

	protected $archivo_config = ''; // Ruta al archivo de configuración

	/**
	* @brief Inicializa el archivo de configuración. Setea las variables necesarias para leer un archivo.
	* @param $p_ruta_archivo. Ruta al archivo de configuración.
	* @return true/false si exsite o no el archivo.
	*/
	public function inicializar($p_ruta_archivo){
		//$this ->debug = $p_debug;
		//$this -> entrar_debug("inicializar");
		
		$this -> set_archivo_archivo_config ($p_ruta_archivo);
		if ($this -> existe($p_ruta_archivo)) {
			//$this -> salir_debug("inicializar");
			//print_r("Archivo de Con");
			return true;
		}
		else {
			//$this->error_debug("El archivo de configuración no existe no se puedo actualizar.");
			print_r("Fail: Ruta del archivo de configuración inexistente: "||$p_ruta_archivo);
			return false;		
		}
		
	}

	/**
	* @brief Lee los datos del archivo de configuración.
	* @see leer().
	* @return un objeto SimpleXMLElement. False si falla.
	*/
	public function leer(){
		$this -> entrar_debug("leer");
		if (file_exists($this -> archivo_config)) {
			return simplexml_load_file($this -> archivo_config);
			
		}
		else {
			$this -> salir_debug("leer");
			return false;		
		}

	}

	/**
	* @brief Verifica si existe el archivo de configuración. 
	* @return boolean que indica si el archivo existe	
	*/
	public function existe(){
		//$this -> entrar_debug("existe");
		return file_exists($this->archivo_config);
		//$this -> salir_debug("existe");
	}
	
	/**
	* @brief Setea la ruta al archivo de configuración. 
	* @return boolean que indica si el archivo existe	
	*/
	public function set_archivo_archivo_config($p_ruta){
		//$this -> entrar_debug("set_salida");
		$this -> archivo_config = $p_ruta;
		//$this -> salir_debug("set_salida");
	}
	
		
}
