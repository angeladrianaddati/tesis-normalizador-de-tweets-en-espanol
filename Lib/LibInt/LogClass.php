<?php
/** @file LogClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase LogClass. Clase que encapsula el manejo de logger.
*/

namespace Lib\LibInt;


/**
 *  LogClass. Clase que encapsula el manejo de logger.
 */
 class LogClass {

	protected $ruta; // Ruta al archivo de log

	/**
	* @brief Setea la variable ruta con la direccion del log en el filserver.
	* @param $p_ruta. Es un string que contiene la ruta en el fileserver.
	* @return nothing.
	*/
	public function set_ruta($p_ruta){
		$this ->ruta = $p_ruta;
	}

	/**
	* @brief Borra el archivo log.
	* @return nothing.
	*/
	public function borrar(){
		if ($this->existe() ==true)
		{
			unlink($this->ruta);
		}
	}

	/**
	* @brief Verifica si existe el archivo de log.
	* @return boolean.
	*/
	public function existe(){
		return file_exists($this->ruta);
	}
	
	/**
	* @brief Devuelve el contenido del archivo de log.
	* @return string.
	*/
	public function leer(){

		if (file_exists($this->ruta)) {
			$string = file_get_contents($this->ruta);
			return $string;			
		}
		else {
			return "";		
		}
	}

	/**
	* @brief Escribe en el archivo de log. No borra lo anteior, agrega el nuevo contenido.
	* @param $p_string. Es la cadena que se va a guardar en el archivo de log.
	* @return nothing.
	*/
	public function escribir($p_string){
		file_put_contents ($this->ruta, $p_string,true); 
	}

}
