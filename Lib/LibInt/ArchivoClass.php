<?php

/** @file ArchivoClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase ArchivoClass. Clase raíz de lectura/escritura en archivos.
*/


namespace Lib\LibInt;

/**
 *  ArchivoClass. Clase raíz de lectura/escritura en archivos.
 */
class ArchivoClass extends BaseAbstractClass{

	protected $ruta; //ruta del file server al archivo

 	/**
	* @brief Setear ruta del archivo.
	* @param $p_ruta. Ruta física al archivo.
	* @return nothing.
	*/	
	public function set_ruta($p_ruta){
		$this -> entrar_debug("set_ruta");
		$this -> ruta = $p_ruta;
		$this -> salir_debug("set_ruta");
	}

 	/**
	* @brief Borrar archivo.
	* @return nothing.
	*/	
	public function borrar(){
		$this -> entrar_debug("borrar");
		unlink($this->ruta);
		$this -> salir_debug("borrar");
	}

 	/**
	* @brief Verifica si existe el archivo.
	* @return nothing.
	*/	
	public function existe(){
		$this -> entrar_debug("existe");
		return file_exists($this->ruta);
		$this -> salir_debug("existe");
	}

 	/**
	* @brief Devuelve un string con el contenido del archivo.
	* @return string/boolean. Devuelve falso si no existe el archivo, sino devuelve el contenido.
	*/	
	public function leer(){
		$this -> entrar_debug("leer");
		if (file_exists($this->ruta)) {
			$string = file_get_contents($this->ruta);
			$this -> salir_debug("leer");
			return  $string;			
		}
		else {
			$this -> salir_debug("leer");
			return false;		
		}

	}

 	/**
	* @brief Devuelve un string con el contenido del archivo.
	* @param $p_string String. Agrega el string al archivo.
	* @return nothing.
	*/
	public function escribir($p_string){
		$this -> entrar_debug("escribir");
			
		file_put_contents ($this->ruta, trim($p_string).PHP_EOL,FILE_APPEND); 
		$this -> salir_debug("escribir");
	}


}
