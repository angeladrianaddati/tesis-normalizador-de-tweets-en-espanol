<?php

/** @file Etapa3Class.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase Etapa3Class. Clase que tiene la abstracción de la etapa 3.
 */

namespace Lib\LibInt;

/**
 *  EtapaClass. Clase que tiene la abstracción de la etapa 3.
 */
class Etapa3Class extends EtapaClass
{

 	protected $auxiliar; // Variable que almacena una instancia de la auxiliar. Esta se usará dentro de los modulos para verificar si los candidatos son palabras correctas.
 
	/**
	* @brief Inicializa la clase.
	* @param $p_salida. Es un objeto de la clase EntradaSalidaClass. A traves de el se registrará la entrada y salida del modulo.
	* @param $p_modulos Listado de módulos a ejecutar.
	* @param $p_namespace. Espacio de nombre de los módulos.
	* @param $p_auxiliar. Es un objeto de la clase EtapaClass. Representa a la auxiliar.
	* @param $p_boolean. Es un objeto de la clase ArchivoIntermedioClass. A traves de el se registrará la entrada y salida del modulo.
	* @param $p_debug. Es un objeto de la clase DebugClass. A traves de el se registraran los eventos del sistema
	* @return nothing.
	*/
 	public function inicializar($p_salida, $p_modulos, $p_namespace, $p_auxiliar, $p_boolean, $p_debug) {
		
		$this -> set_debug($p_debug);
		$this -> set_namespace($p_namespace);
		$this -> set_auxiliar($p_auxiliar);
		$this -> set_salida($p_salida);
		$this -> set_acent_sensitive($p_boolean);
		$this -> set_modulos($p_modulos);
	}
	
 	/**
	* @brief Método abstracto. El programá principal llamará a este método. 
	* @return nothing.
	*/
	public function ejecutar(){
		$this -> entrar_debug("ejecutar");
		foreach ($this->modulos as $modulo){
			///print_r(' '.$modulo->nombre);
			//$tiempo = time();
			//eval('$i = new '.$this->namespace.$modulo.';');
			//$i-> inicializar ($this->salida,$this->acent_sensitive, $this->debug);
			//$i-> set_lazzy($this->lazzy);
			$modulo ->ejecutar();
			//print_r("\n  Finalizando modulo: ".$this->namespace.$modulo." (".($this->time_elapsed(time()-$tiempo)).")");
		}
		$this -> salir_debug("ejecutar");
	}
	
	/**
	* @brief Setea la variable modulos con el listado de los modulos a ejecutar.
	* @param $p_modulos. Arregloc on el formato: array(NombreModulo1, NombreModulo2, ...)
	* @return nothing.
	*/
	public function set_modulos($p_modulos){
		$this -> entrar_debug("set_modulos");
		$this -> modulos = array();
		foreach ($p_modulos as $modulo){
			//print_r("\n  Comenzando modulo: ".$this->namespace.$modulo);
			$tiempo = time();
			eval('$i = new '.$this->namespace.$modulo.';');
			$i-> inicializar ($this->salida,$this->acent_sensitive,$this->auxiliar, $this->debug);
			//$i-> nombre = $modulo;
			$this ->modulos[]= $i;
			
		}
		$this -> salir_debug("set_modulos");
	}

	/**
	* @brief Setea la variable auxiliar con un objeto del tipo EtapaClass.
	* @param $p_auxiliar. Es un objeto de la clase EtapaClass. Representa la auxiliar y se usará en los módulos para verificar si las palabras candidatas son IV.
	* @return nothing.
	*/
	public function set_auxiliar($p_auxiliar){
		$this -> entrar_debug("set_auxiliar");
		$this ->auxiliar = $p_auxiliar;
		foreach ($this->modulos as &$modulo){
			$modulo->set_auxiliar($p_auxiliar);
		}
		$this -> salir_debug("set_auxiliar");
	}


}

