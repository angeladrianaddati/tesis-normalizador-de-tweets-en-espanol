<?php
/** @file EntradaSalidaAbstractClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase EntradaSalidaAbstractClass. Clase abstracta que define la interfaz y el comportamiento para las clases que se usaran para generar la salida del preprocesador. Clase abstracta que define la interfaz y el comportamiento para las clases que se usaran para generar la salida del preprocesador. 
 */
namespace Lib\LibInt;

/**
 *  EntradaSalidaAbstractClass. Clase abstracta que define la interfaz y el comportamiento para las clases que se usaran para generar la salida del preprocesador. Clase abstracta que define la interfaz y el comportamiento para las clases que se usaran para generar la salida del preprocesador. 
 */
abstract class EntradaSalidaAbstractClass extends BaseAbstractClass {

	protected $salida; //Salida. La salida va a depender de la subclase. Podrá ser en memoria o una ruta a un archivo.;

	/**
	* @brief Inicializa la clase.
	* @param $p_entrada. Representa la variable de entrada que se utilizará para la inicialización.
	* @param $p_salida. Representa la variable sobre la que se generará la incialización.  
	* @param $p_debug. Es el objeto debug responsable del logging. 
	* @return nothing.
	*/
	abstract public function inicializar($p_entrada, $p_salida,  $p_debug);
	
	/**
	* @brief Borra la salida. 
	* @return nothing.
	*/
	abstract public function borrar();

	/**
	* @brief Verifica si existe el objeto salida. 
	* @return boolean que indica si el objeto existe o no.
	*/
	abstract public function existe();

	/**
	* @brief Lee la salida y la devuelve. 
	* @return devuelve el objeto de salida.
	*/
	abstract public function leer();

	/**
	* @brief Escribe en la salida. Recibe un objeto y lo vuelca en la salida.
	* @param $p_obj_salida. Objeto de salida. 
	* @return false en caso de error.
	*/
	abstract public function escribir($p_obj_salida);

	/**
	* @brief Setea la variable salida.
	* @param $p_salida representa la saluda.
	* @return nothing.
	*/
	public function set_salida($p_salida){
		$this -> entrar_debug("set_salida");
		$this -> salida = $p_salida;
		$this -> salir_debug("set_salida");
	}

}
