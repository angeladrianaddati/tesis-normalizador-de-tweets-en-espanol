<?php

/** @file BaseAbstractClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase BaseAbstractClass. Clase base para el resto de las clases del sistema.
*/

namespace Lib\LibInt;

/**
 *  BaseAbstractClass. Clase base para el resto de las clases del sistema.
 */

abstract class BaseAbstractClass
{
	protected $debug = null; // Objeto del tipo DebugClass.
    
	/**
	* @brief Setea la variable debug con un objeto del tipo DebugClass.
	* @param $p_debug. Es un objeto de la clase DebugClass. A traves de el se registrará el logging de la aplicación.
	* @return nothing.
	*/
	public function set_debug($p_debug){

		$this -> entrar_debug("set_debug");
		$this ->debug = $p_debug;
		$this -> salir_debug("set_debug");
	}

	/**
	* @brief Llama al objeto debug para registrar, si el debug está en modo exhaustivo, que se ingreso al método. 
	* @param $p_string. Mensaje a registrar en el debug.
	* @return nothing.
	*/
	protected function entrar_debug($p_string) {

		if ($this ->debug!= null) {
			$this ->debug->entrar(get_class($this)."->".$p_string);		
		}
	} 

	/**
	* @brief Llama al objeto debug para registrar, si el debug está en modo exhaustivo, que se finalizó el método. 
	* @param $p_string. Mensaje a registrar en el debug.
	* @return nothing.
	*/
	protected function salir_debug($p_string) {

		if ($this ->debug!= null) {
			$this ->debug-> salir(get_class($this)."->".$p_string);		
		}
	} 

	/**
	* @brief Llama al objeto debug para registrar un warning en la aplicación.
	* @param $p_string. Mensaje a registrar en el debug.
	* @return nothing.
	*/
	protected function warn_debug($p_string) {

		if ($this ->debug!= null) {
			$this ->debug->warn(get_class($this)."->".$p_string);		
		}
	} 

	/**
	* @brief Llama al objeto debug para registrar un error en la aplicación. 
	* @param $p_string. Mensaje a registrar en el debug.
	* @return nothing.
	*/
	protected function error_debug($p_string) {
		if ($this ->debug!= null) {
			$this ->debug->error(get_class($this)."->".$p_string);		
		}
	} 


}

