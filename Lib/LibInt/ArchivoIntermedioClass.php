<?php

/** @file ArchivoIntermedioClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase ArchivoIntermedioClass. Clase responsable de la lectura y escritura en los archivos intermedios
*/

namespace Lib\LibInt;

/**
 *  ArchivoIntermedioClass. Clase responsable de la lectura y escritura en los archivos intermedios. Estos archivos serán la entrada y salida de los módulos. 
 */

class ArchivoIntermedioClass extends EntradaSalidaAbstractClass{

	protected $archivo_salida = ""; // Ruta al archivo de salida

	/**
	* @brief Inicializa el archivo intermedio. Lee un archivo de entrada y lo vuelca en un archivo intermedio sobre el que se trabajará.
	* @param $p_ruta_entrada. Ruta al archivo de entrada.
	* @param $p_ruta_salida. Ruta al archivo de salida. 
	* @param $p_debug. Es el objeto debug responsable del logging. 
	* @return nothing.
	*/
	public function inicializar($p_ruta_entrada, $p_ruta_archivo_intermedio, $p_debug){
	
		$this ->debug = $p_debug;
		$this -> entrar_debug("inicializar");
		if (file_exists($p_ruta_entrada)) {
			$e_xml = simplexml_load_file($p_ruta_entrada);
		}
		else {
			$this -> warn_debug("inicializar: Ruta al archivo de entrada no existe.");
			$this -> salir_debug("inicializar");
			return false;		
		}

		$v_xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" '. 'standalone="yes"?><preprocesador/>');

		foreach($e_xml->mensaje as $mensaje)
		{
			$nuevo_nodo = $v_xml -> addChild("mensaje","");
			$nuevo_nodo = $nuevo_nodo -> addChild("token","");
			//print_r($mensaje);
			$nuevo_nodo -> addChild("palabra",(string)$mensaje);
			$nuevo_nodo -> addChild("iv","");
			//$nuevo_nodo -> addChild("candidato","");
			$nuevo_nodo -> addChild("seleccion","");
			$nuevo_nodo -> addChild("capitalizado","");
		}


		$this -> set_salida($p_ruta_archivo_intermedio);


		if ($v_xml -> asXML($this -> salida)==false)
		{
			$this->error_debug("El archivo de e/s no se puedo actualizar.");					
		};
		
	}

	/**
	* @brief Lee los datos de entrada del archivo intermedio.
	* @return arreglo con el formato array(mensaje1(token(palabra,iv,array(candidato1, candidato2,...), seleccion,capitalizado)), mensaje2(...), ...)
	*/
	public function leer(){
		$this -> entrar_debug("leer");
		if (file_exists($this -> salida)) {
			$v_xml = simplexml_load_file($this -> salida);
			$arr0 = array();
			foreach ($v_xml->mensaje as $mensaje)
			{
				$arr1 = array();
				foreach ($mensaje->token as $token){
					$arr2 = array();
					if ($token->candidato!=null){
						foreach ($token->candidato as $candidato){
							$arr3 = array();
							if ($candidato->palabra!=null){
								$arr3[0] = (string)$candidato->palabra;
							}
							if ($candidato->normalizador!=null){
								$arr3[1]=(string)$candidato->normalizador;
							}
							array_push($arr2,$arr3);
						}
					}				
					array_push($arr1 ,array((string)$token->palabra,(boolean)(string)$token->iv,$arr2,(string)$token->seleccion, (string)$token->capitalizado));
				}
				if (count($arr1)>0) array_push($arr0,$arr1); 
			}
			$this -> salir_debug("leer");
			return $arr0;			
		}
		else {
			$this -> salir_debug("leer");
			return false;		
		}

	}

	/**
	* @brief Escribe en un archivo intermedio. Recibe un arreglo de entrada y lo vuelca en el archivo intermedio.
	* @param $p_array. Arreglo con el formato array(mensaje1(token(palabra,iv,array(candidato1, candidato2,...), seleccion,capitalizado)), mensaje2(...), ...)
	* @return false en caso de error.
	*/
	public function escribir($p_array){
		$this -> entrar_debug("escribir");
		if (!file_exists($this -> salida)) {
			$v_xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" '. 'standalone="yes"?><preprocesador/>');
			if ($v_xml -> asXML($this -> salida)==false){
				$this->error_debug("El archivo de e/s no se puedo actualizar.");					
			};
		};
		
			$xml = simplexml_load_file($this -> salida);
			unset($xml->mensaje);
			//unset($xml->preprocesador);
			$nuevo_nodo0  = $xml;
			

			foreach ($p_array as $mensaje){
				$nuevo_nodo1  = $nuevo_nodo0 -> addChild("mensaje","");					
				foreach ($mensaje as $token){
					$nuevo_nodo2 = $nuevo_nodo1 -> addChild("token","");

					if (array_key_exists(0,$token)) {
						$nuevo_nodo2 -> addChild("palabra",(string)$token[0]);
					} else {
						$nuevo_nodo2 -> addChild("palabra","");
						print_r("ERROR"); exit(1);
					}
			
					if (array_key_exists(1,$token)) {
						$nuevo_nodo2 -> addChild("iv",(string)$token[1]);
					} else {
						$nuevo_nodo2 -> addChild("iv","");
					}
					
					if (array_key_exists(2,$token)) {
						foreach ($token[2] as $candidato) {
							$nuevo_nodo3 = $nuevo_nodo2 -> addChild ("candidato","");
							if (array_key_exists(0,$candidato)) {
								$nuevo_nodo3 -> addChild ("palabra",$candidato[0]);
							}
							if (array_key_exists(1,$candidato)) {
								$nuevo_nodo3 -> addChild ("normalizador",$candidato[1]);
							}
							
						}
					}
					
					if (array_key_exists(3,$token)) {
						$nuevo_nodo2 -> addChild("seleccion",(string)$token[3]);
					} else {
						$nuevo_nodo2 -> addChild("seleccion","");
					}


					if (array_key_exists(4,$token)) {
						$nuevo_nodo2 -> addChild("capitalizado",(string)$token[4]);
					} else {
						$nuevo_nodo2 -> addChild("capitalizado","");
					}

				}			
			};
			
			if ($xml -> asXML($this -> salida)==false) {
					$this->error_debug("El archivo de e/s no se puedo actualizar.");
					return false;					
				};

		$this -> salir_debug("escribir");
	}		

	/**
	* @brief Borra el archivo intermedio. 
	* @return nothing.
	*/
	public function borrar(){
		$this -> entrar_debug("borrar");
		unlink($this->salida);
		$this -> salir_debug("borrar");
	}

	/**
	* @brief Verifica si existe el archivo intermedio. 
	* @return boolean que indica si el archivo existe	
	*/
	public function existe(){
		$this -> entrar_debug("existe");
		return file_exists($this->salida);
		$this -> salir_debug("existe");
	}

	/**
	* @brief Setea la ruta al archivo de salida. 
	* @param $p_ruta. String con la ruta al archivo de salida.
	* @return nothing.	
	*/
		
	public function set_archivo_salida($p_ruta){
		$this -> entrar_debug("set_salida");
		$this -> archivo_salida = $p_ruta;
		$this -> salir_debug("set_salida");
	}
	
	/**
	* @brief Escribe en un archivo intermedio. Recibe un arreglo de entrada y lo vuelca en el archivo intermedio.
	* @param $p_array. Arreglo con el formato array(mensaje1(token(palabra,iv,array(candidato1, candidato2,...), seleccion,capitalizado)), mensaje2(...), ...)
	* @return false en caso de error.
	*/
	public function generar_salida(){
		$this -> entrar_debug("set_salida");
		$intermedio = $this ->leer();
		
		$this -> entrar_debug("escribir");
		if (!file_exists($this -> archivo_salida)) {
			$v_xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" '. 'standalone="yes"?><mensajes/>');
			if ($v_xml -> asXML($this -> archivo_salida)==false){
				$this->error_debug("El archivo de e/s no se puedo actualizar.");					
			};
		};
		
			$xml = simplexml_load_file($this -> archivo_salida);
			unset($xml->mensaje);
			//unset($xml->preprocesador);
			$nuevo_nodo0  = $xml;
			
			foreach ($intermedio as $mensaje){
				$tmp = "";
				$tmp_palabra = "";	
				$tmp_palabra_ant = "";
				//print_r($mensaje);			
				foreach ($mensaje as $token){
					$tmp_palabra_ant = $tmp_palabra;
					if ($token[1]== true or count($token[2])<1)
						$tmp_palabra = $token[0];
					else
						$tmp_palabra = $token[2][0][0];
						
					if (in_array($tmp_palabra_ant,array('¿','¡')))
						$tmp .= $tmp_palabra;
					elseif (in_array($tmp_palabra_ant, array('.', ',', ':', '...', '?',';','!')))
						$tmp .= " ".$tmp_palabra;
					elseif (in_array($tmp_palabra, array('.', ',', ':', '...', '?',';','!')))
						$tmp .= $tmp_palabra;		
					else
						$tmp .=" ".$tmp_palabra;

					}
				$nuevo_nodo0 -> addChild("mensaje",trim($tmp));		
			}
				
					
			if ($xml -> asXML($this -> archivo_salida)==false) {
					$this->error_debug("El archivo de e/s no se puedo actualizar.");
					return false;					
				};

		$this -> salir_debug("escribir");

	}
	
}
