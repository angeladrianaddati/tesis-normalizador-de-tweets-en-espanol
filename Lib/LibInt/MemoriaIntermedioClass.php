<?php
/** @file MemoriaIntermedioClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase MemoriaIntermedioClass. Clase responsable de la lectura y escritura en un buffer de memoria
*/
namespace Lib\LibInt;

/**
 *  MemoriaIntermedioClass. Clase responsable de la lectura y escritura en un buffer de memoria, manteniendo un formato similar al archivo intermedio.
 */

class MemoriaIntermedioClass extends EntradaSalidaAbstractClass{


	/**
	* @brief Inicializa la clase.
	* @param $p_entrada. Ruta al archivo de entrada.
	* @param _. Este parametro no se usa, se mantiene para mantener la interfaz. 
	* @param $p_debug. Objeto encargado del loggin de la aplicación. 
	* @return nothing.
	*/
	public function inicializar($p_entrada,$p, $p_debug){
		$this ->debug = $p_debug;
		$this -> entrar_debug("leer");

		$this -> salida = $p_entrada;

		$this -> salir_debug("leer");
	
	}

	/**
	* @brief Devuelve los datos que se encuentran en la variable de salida. 
	* @return variable de salida.
	*/
	public function leer(){
		$this -> entrar_debug("leer");
		$this -> salir_debug("leer");
		return $this -> salida;


	}


	/**
	* @brief Escribe en la variable de salida. 
	* @param $p_array. Escribe en la variable de salida. 
	* @return nothing.
	*/
 	public function escribir($p_array){
		$this -> entrar_debug("escribir");
		$this -> salida = $p_array;
		$this -> salir_debug("escribir");
	}

	/**
	* @brief Borra la variable de salida salida. 
	* @return nothing.
	*/
	public function borrar(){
		$this -> entrar_debug("borrar");
		$this -> salida = null;
		$this -> salir_debug("borrar");
	}

	/**
	* @brief Escribe en la variable de salida. 
	* @return nothing.
	*/
 	public function existe(){
		$this -> entrar_debug("existe");
		$this -> salir_debug("existe");
		return $this -> salida != null;
	}

}
