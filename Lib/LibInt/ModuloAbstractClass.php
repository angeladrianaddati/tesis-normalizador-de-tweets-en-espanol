<?php

/** @file ModuloAbstractClass.php
* @Author Angel Adrián Addati
* @date 02/04/2019
* @brief Contiene la definición de la clase ModuloAbstractClass. Clase base para los módulos.
*/

namespace Lib\LibInt;

/**
 *  ModuloAbstractClass. Clase base para los módulos.
 */
abstract class ModuloAbstractClass extends BaseAbstractClass
{

 	protected $salida; // Objeto que apunta al objeto que almacenará la salida. Este archivo servirá de entrada y salida;
 	protected $acent_sensitive;  // Indica si se tendrán en cuenta los acentos o no;
	protected $lazzy= false;   // Indica si se tendrán en cuenta los acentos o no;
  	protected $fonetic= false; // Indica si la comparación será fonetica
 	protected $levenshtein= false; // Indica si la comparación será por distancia de edicion
 		
	/**
	* @brief Inicializa la clase.
	* @param $p_salida Es un objeto de la clase EntradaSalidaClass. A traves de el se registrará la entrada y salida del modulo.
	* @param $p_acent_sensitive. Booleando que indica si el módulo será o no sensitivo a los acentos.
	* @param $p_bug. Es un objeto de la clase DebugClass. A traves de el se registraran los eventos del sistema
	* @return nothing.
	*/
 	public function inicializar($p_salida, $p_acent_sensitive, $p_debug) {

		$this -> entrar_debug("inicializar");

		$this -> set_debug($p_debug);
		$this -> set_salida($p_salida);
		$this -> set_acent_sensitive($p_acent_sensitive);
		$this -> set_lazzy(false);
		
		$this -> salir_debug("inicializar");
	}

   
 	/**
	* @brief Método abstracto. El programá principal llamará a este método. 
	* @return nothing.
	*/
	abstract public function ejecutar();

	/**
	* @brief Setea la variable archivo_intermedio con un objeto del tipo EntradaSalidaClass.
	* @param $p_salida. Es un objeto de la clase EntradaSalidaClass. A traves de el se registrará la entrada y salida del modulo.
	* @return nothing.
	*/
	public function set_salida($p_salida){
		$this -> entrar_debug("set_salida");
		$this -> salida = $p_salida;
		$this -> salir_debug("set_salida");
	}

	/**
	* @brief Setea la variable acent_sensitive que indica si se debe tener en cuenta o no los acentos.
	* @param $p_boolean. Es un booleano.
	* @return nothing.
	*/
	public function set_acent_sensitive($p_boolean){
		$this -> entrar_debug("set_acent_sensitive");
		$this ->acent_sensitive = $p_boolean;
		$this -> salir_debug("set_acenti_sensitive");
	}
	
	/**
	* @brief Setea la variable acent_sensitive que indica si se debe tener en cuenta o no los acentos.
	* @param $p_boolean. Es un booleano.
	* @return nothing.
	*/
	public function set_lazzy($p_boolean){
		$this -> entrar_debug("set_acent_sensitive");
		$this ->lazzy = $p_boolean;
		$this -> salir_debug("set_acenti_sensitive");
	}
	
	/**
	* @brief Setea la variable acent_sensitive que indica si se tendrán en cuenta los acentos o no.
	* @param $p_boolean. Booleano.
	* @return nothing.
	*/
	public function set_fonetic($p_boolean){
		$this -> entrar_debug("set_archivo_intermedio");
		$this -> fonetic = $p_boolean;
		$this -> salir_debug("set_archivo_intermedio");
	}

	/**
	* @brief Setea la variable acent_sensitive que indica si se tendrán en cuenta los acentos o no.
	* @see set_acent_sensitive().
	* @param $p_boolean. Booleano.
	* @return nothing.
	*/
	public function set_levenshtein($p_boolean){
		$this -> entrar_debug("set_archivo_intermedio");
		$this -> levenshtein= $p_boolean;
		$this -> salir_debug("set_archivo_intermedio");
	}


}

